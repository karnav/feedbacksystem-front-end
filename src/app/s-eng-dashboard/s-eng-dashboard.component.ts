import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';

import { ProblemService } from '../services/problem.service';

import { ProblemDetail, ProblemStatus } from '../shared/problemdetails';
import { ProblemTransaction } from '../shared/problemtransactions';
import { User } from '../shared/user';
import { Mail } from '../shared/mail';
import { AuthService } from '../services/auth.service';
import { MatSnackBar, MatPaginator, MatTable, MatTableDataSource, MatSort, MatDialog, MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-s-eng-dashboard',
  templateUrl: './s-eng-dashboard.component.html',
  styleUrls: ['./s-eng-dashboard.component.scss']
})
export class SEngDashboardComponent implements OnInit {

  problems: ProblemDetail[];
  problem = new ProblemDetail();
  problemtransaction= new ProblemTransaction();
  users: User[];
  user: User;
  startDate: Date;
  endDate: Date;
  errMess: string;
  Engineers = [];
  selectedValue: string; // For selecting engineers
  editCardId: any = '';
  selectedValue1 = 'Registered'; // For selecting problem status
  problemStatus = ProblemStatus;
  Eid: string;
  mail = new Mail();
  eng: string;
  engusername: string;
  problemCreatedDate: Date;

  // paginator input
  length: number;

  // table columns
  displayedColumns = ['createdAt', 'problem_title', 'problem_status', 'customer_name', 'engineer', 'updatedAt', 'actions'];
  dataSource: MatTableDataSource<ProblemDetail>;

  constructor(
    private router: Router,
    private problemService: ProblemService,
    private authService: AuthService,
    private snackBar: MatSnackBar
  ) { }

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  ngOnInit() {
    const usertype = 'Engineer';
    this.authService.getSelectedUsers(usertype).subscribe(users => {
      this.users = users;
      const length = this.users.length;
      // console.log('users', this.users);
      for (let i = 0; i < length; i++) {
        this.Engineers[i] = {value: this.users[i]._id, viewValue: this.users[i].username};
      }
      // console.log('Engineers: ', this.Engineers);
    },
    errmess => {
      this.errMess = <any>errmess;
      this.snackBar.open(this.errMess, 'Ok', {
        duration: 4000
      });
    });

    this.eng = localStorage.getItem('Id');
    const eid = this.eng.substr(1);
    const EID = eid.slice(0, -1);
    this.authService.getUserById(EID).subscribe(user => {
      this.user = user;
      this.engusername = this.user.firstname + ' ' + this.user.lastname;
    },
    errmess => {
      this.errMess = <any>errmess;
      this.snackBar.open(this.errMess, 'Ok', {
        duration: 4000
      });
    });
  }

  dashboard() {
    this.router.navigate(['/sengdashboard']);
  }

  worklog() {
    this.router.navigate(['/worklog']);
  }

  onSubmit() {
    // var startdate =this.startDate;
    const startdate = new Date(this.startDate);
    const sdate = startdate.toISOString();
    // console.log('startdate',sdate);

     const Id = this.selectedValue;
    //  console.log(Id);
    // const tid = Id.substr(1);
    // const id = tid.slice(0, -1);
    

    // var enddate = this.endDate;
    const enddate = new Date(this.endDate);
    const edate = enddate.toISOString();
    // console.log(startdate);
    // console.log(enddate);
    // console.log('enddate', edate);
    
    



    const problemstatus = '';
    this.problemService.getProblemsbyDate(sdate, edate, Id, problemstatus).subscribe( problems => {
      this.problems = problems;
      // console.log('Problems:', problems);
      this.dataSource = new MatTableDataSource(this.problems);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
      this.length = this.problems.length;
      // console.log('length', length);
    },
    errmess => {
      this.errMess = <any>errmess;
      this.snackBar.open(this.errMess, 'Ok', {
        duration: 4000
      });
    });

  }

  onEdit(id: string) {
    //this.editable = !this.editable ;
    this.editCardId = id;
    // console.log(this.editCardId);
  }

  onSave(id: string) {
    // console.log('Id', id);
    const data: string = this.selectedValue1;
    // console.log('DaTa', data);
    // this.problem = new ProblemDetail();
    // this.problem.problem_status = data;
    // console.log('problem', this.problem);
    const startdate = new Date(this.startDate);
    const sdate = startdate.toISOString();
    // console.log(startdate);
    const enddate = new Date(this.endDate);
    const edate = enddate.toISOString();
    // console.log(startdate);
    const Id = this.selectedValue;
    // console.log(Id);

    //Senior Engineer Id
    const Sid = localStorage.getItem('Id');
    // console.log('Eng Id', Sid);
    const sid = Sid.substr(1);
    const SID = sid.slice(0, -1);

    this.problemService.getProblem(id).subscribe(problem => {
      this.problem = problem;
      // console.log('Problem#', this.problem);
      this.Eid = this.problem.engineer;
      // console.log('Eid#', this.Eid);
      this.problem.problem_status = data;
      // console.log('selected problemstatus', this.problem.problem_status);
      this.problemService.updateProblem(id, this.problem, this.Eid).subscribe(problem => {
        this.problem = problem;
        // console.log('PrObLeM', problem);

        this.problemCreatedDate = new Date(this.problem.createdAt);
        const year = this.problemCreatedDate.getFullYear();
        const month = this.problemCreatedDate.getMonth() + 1;
        const day = this.problemCreatedDate.getDate();
        const date = day + '/' + month + '/' + year;
  
        //customer Id 
        const Cid = this.problem.customer;
        // console.log('Customer Id', Cid);
  
        //engineer Id
        const EID = this.problem.engineer;
        // console.log('Engineer Id', EID);

      // geting user for sending mail
      this.authService.getUserById(Cid).subscribe(user => {
        this.user = user;
        // console.log(this.user);
        if(user) {
          this.mail.subject = 'Problem status updated';
          this.mail.message = 'Hello ' + this.user.username + ', your problem having title \'' + this.problem.problem_title + '\' which was registered on \'' + date + '\' has been taken by our senior engineer. Our senior engineer named \'' + this.engusername + '\' has set status of your problem to \'' + this.problem.problem_status + '\'!';
          //For sending mail
          this.problemService.sendMail(this.user, this.mail).subscribe(data => {
            // console.log("Mail", data);
            this.snackBar.open("Mail sent successfullly!", "Ok", {
              duration: 3000
            });
          },
          error => {
            // console.log(error.status, error.message);
            this.snackBar.open("Failed to send mail problem", "Ok", {
              duration: 3000
            });
          });
        }
      },
      errmess => {
        this.errMess = <any>errmess;
      });
  
        this.problemService.postProblemtransactionBySeniorEng(this.problem,Sid, EID, Cid).subscribe(problemtransaction => {
          this.problemtransaction = problemtransaction;
          // console.log('problemtransaction:',this.problemtransaction);
        });
  
        id = '';
        this.editCardId = '';
        // console.log('id', id);
        this.snackBar.open("Problem edited successfully!", "Ok", {
          duration: 2000
        });
        const problemstatus = '';
        this.problemService.getProblemsbyDate(sdate, edate, Id, problemstatus).subscribe( problems => {
          this.problems = problems;
          // console.log('Problems:', problems);
        },
        errmess => {
          this.errMess = <any>errmess;
          this.snackBar.open(this.errMess, 'Ok', {
            duration: 4000
          });
        });
        this.onSubmit();
      },
      errmess => {
        this.errMess = <any>errmess;
        this.snackBar.open("Something went wrong!", "Ok", {
          duration: 2000
        });
      });

    });
    // console.log('Eid', this.Eid);
    
   }

   onCancel() {
    
    this.editCardId = null;
  }
  
  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

}
