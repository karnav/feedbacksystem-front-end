import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SEngDashboardComponent } from './s-eng-dashboard.component';

describe('SEngDashboardComponent', () => {
  let component: SEngDashboardComponent;
  let fixture: ComponentFixture<SEngDashboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SEngDashboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SEngDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
