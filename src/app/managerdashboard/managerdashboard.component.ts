import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { ProblemService } from '../services/problem.service';

import { ProblemDetail } from '../shared/problemdetails';
import { ProblemTransaction } from '../shared/problemtransactions';
import { User } from '../shared/user';

@Component({
  selector: 'app-managerdashboard',
  templateUrl: './managerdashboard.component.html',
  styleUrls: ['./managerdashboard.component.scss']
})
export class ManagerdashboardComponent implements OnInit {

  today_end: Date = new Date();
  today_start: Date = new Date();
  problems: ProblemDetail[];
  problem: ProblemDetail = new ProblemDetail();
  week: Date = new Date(); // Used to get problems in last week
  month: Date = new Date(); // Used to get problems in last month
  sixmonth: Date = new Date(); // Used to get problems in last 6 months
  yesterday_start: Date = new Date();
  yesterday_end: Date = new Date();
  length: number;// For problems registered today
  length_week: number; // For problems registered Last week
  length_month: number; // For problems registered in last month
  length_sixmonths: number; // For problems registered in last six months
  r_t_length: number;
  r_w_length: number;
  r_m_length: number;
  r_sm_length: number;
  wa_t_length: number;
  wa_w_length: number;
  wa_m_length: number;
  wa_sm_length: number;
  wp_t_length: number;
  wp_w_length: number;
  wp_m_length: number;
  wp_sm_length: number;
  e_t_length: number;
  e_w_length: number;
  e_m_length: number;
  e_sm_length: number;
  c_t_length: number;
  c_w_length: number;
  c_m_length: number;
  c_sm_length: number;
  r_t_id: string;
  r_w_id: string;
  r_m_id: string;
  r_sm_id: string;
  wa_t_id: string;
  wa_w_id: string;
  wa_m_id: string;
  wa_sm_id: string;
  wp_t_id: string;
  wp_w_id: string;
  wp_m_id: string;
  wp_sm_id: string;
  e_t_id: string;
  e_w_id: string;
  e_m_id: string;
  e_sm_id: string;
  c_t_id: string;
  c_w_id: string;
  c_m_id: string;
  c_sm_id: string;

  constructor(
    private router: Router,
    private problemService: ProblemService
  ) { }

  ngOnInit() {
    // current date 23.59 hrs
    this.today_end.setHours(23, 59, 59, 999);
    const today_end = this.today_end.toISOString();
    // console.log('today end', today_end);
    // current date 00.00hrs
    this.today_start.setHours(0, 0, 0, 0);
    const today_start = this.today_start.toISOString();
    // console.log('today start', today_start);
    // Yesterday 00.00hrs
    this.yesterday_start.setDate(this.today_start.getDate() - 1);
    this.yesterday_start.setHours(0, 0, 0, 0);
    const yesterday_start = this.yesterday_start.toISOString();
    // console.log('yesterday start', yesterday_start);
    // Yesterday 23.59hrs
    this.yesterday_end.setDate(this.today_end.getDate() - 1);
    this.yesterday_end.setHours(23, 59, 59, 999);
    const yesterday_end = this.yesterday_end.toISOString();
    // console.log('yesterday end', yesterday_end);
    // 7 days span time from today
    this.week.setDate(this.today_end.getDate() - 6);
    this.week.setHours(0, 0, 0, 0);
    const week = this.week.toISOString();
    // console.log('week', week);

    // Last month
    this.month.setMonth(this.today_end.getMonth() - 1);
    this.month.setHours(0, 0, 0, 0);
    const month = this.month.toISOString();
    // console.log('month', month);

    // Last six months
    this.sixmonth.setMonth(this.today_end.getMonth() - 5);
    this.sixmonth.setHours(0, 0, 0, 0);
    const sixmonth = this.sixmonth.toISOString();
    // console.log('sixmonth', sixmonth);

    const Mid = localStorage.getItem('Id');

    // Problems registered today
    this.problemService.getTodaysproblem(today_start, today_end, Mid).subscribe(problems => {
      this.problems = problems;
      this.length = this.problems.length;
      // console.log('Today problems', this.problems);

      // Registered problems
      const r_problemstatus = 'Registered';
      this.problemService.getTodaysproblemWithProblemStatus(today_start, today_end, Mid, r_problemstatus).subscribe(rproblems => {
        this.problems = rproblems;
        this.r_t_length = this.problems.length;
        // console.log('Today registered problems', this.problems);
      });

      // Waiting for approval problems
      const w_problemstatus = 'Waiting_for_Approval';
      this.problemService.getTodaysproblemWithProblemStatus(today_start, today_end, Mid, w_problemstatus).subscribe(wproblems => {
        this.problems = wproblems;
        this.wa_t_length = this.problems.length;
        // console.log('Today Waiting for approval problems', this.problems);
      });

      // Work in progress problems
      const wp_problemstatus = 'Work_in_Progress';
      this.problemService.getTodaysproblemWithProblemStatus(today_start, today_end, Mid, wp_problemstatus).subscribe(wpproblems => {
        this.problems = wpproblems;
        this.wp_t_length = this.problems.length;
        // console.log('Today Waiting for approval problems', this.problems);
      });

      // Escaleted problems
      const e_problemstatus = 'Escaleted';
      this.problemService.getTodaysproblemWithProblemStatus(today_start, today_end, Mid, e_problemstatus).subscribe(eproblems => {
        this.problems = eproblems;
        this.e_t_length = this.problems.length;
        // console.log('Today Waiting for approval problems', this.problems);
      });

      // Escaleted problems
      const c_problemstatus = 'Close';
      this.problemService.getTodaysproblemWithProblemStatus(today_start, today_end, Mid, c_problemstatus).subscribe(cproblems => {
        this.problems = cproblems;
        this.c_t_length = this.problems.length;
        // console.log('Today Waiting for approval problems', this.problems);
      });
    });
    
    // Problems registered in last week
    this.problemService.getTodaysproblem(week, today_end, Mid).subscribe(problems => {
      this.problems = problems;
      this.length_week = this.problems.length;
      // console.log('Last week problems:', this.problems);

      // Registered problems
      const r_problemstatus = 'Registered';
      this.problemService.getTodaysproblemWithProblemStatus(week, today_end, Mid, r_problemstatus).subscribe(rproblems => {
        this.problems = rproblems;
        this.r_w_length = this.problems.length;
        // console.log('Last week registered problems', this.problems);
      });

      // Waiting for approval problems
      const w_problemstatus = 'Waiting_for_Approval';
      this.problemService.getTodaysproblemWithProblemStatus(week, today_end, Mid, w_problemstatus).subscribe(wproblems => {
        this.problems = wproblems;
        this.wa_w_length = this.problems.length;
        // console.log('Last week Waiting for approval problems', this.problems);
      });

      // Work in progress problems
      const wp_problemstatus = 'Work_in_Progress';
      this.problemService.getTodaysproblemWithProblemStatus(week, today_end, Mid, wp_problemstatus).subscribe(wpproblems => {
        this.problems = wpproblems;
        this.wp_w_length = this.problems.length;
        // console.log('Last week Waiting for approval problems', this.problems);
      });

      // Escaleted problems
      const e_problemstatus = 'Escaleted';
      this.problemService.getTodaysproblemWithProblemStatus(week, today_end, Mid, e_problemstatus).subscribe(eproblems => {
        this.problems = eproblems;
        this.e_w_length = this.problems.length;
        // console.log('Today Waiting for approval problems', this.problems);
      });

      // Closed problems
      const c_problemstatus = 'Close';
      this.problemService.getTodaysproblemWithProblemStatus(week, today_end, Mid, c_problemstatus).subscribe(cproblems => {
        this.problems = cproblems;
        this.c_w_length = this.problems.length;
        // console.log('Today Waiting for approval problems', this.problems);
      });
    });

    // Problems registered in last month
    this.problemService.getTodaysproblem(month, today_end, Mid).subscribe(problems => {
      this.problems = problems;
      this.length_month = this.problems.length;
      // console.log('Last month problems:', this.problems);

      // Registered problems
      const r_problemstatus = 'Registered';
      this.problemService.getTodaysproblemWithProblemStatus(month, today_end, Mid, r_problemstatus).subscribe(rproblems => {
        this.problems = rproblems;
        this.r_m_length = this.problems.length;
        // console.log('Last month registered problems', this.problems);
      });

      // Waiting for approval problems
      const w_problemstatus = 'Waiting_for_Approval';
      this.problemService.getTodaysproblemWithProblemStatus(month, today_end, Mid, w_problemstatus).subscribe(wproblems => {
        this.problems = wproblems;
        this.wa_m_length = this.problems.length;
        // console.log('Last month Waiting for approval problems', this.problems);
      });

      // Work in progress problems
      const wp_problemstatus = 'Work_in_Progress';
      this.problemService.getTodaysproblemWithProblemStatus(month, today_end, Mid, wp_problemstatus).subscribe(wpproblems => {
        this.problems = wpproblems;
        this.wp_m_length = this.problems.length;
        // console.log('Last month Work in progress problems', this.problems);
      });

      // Escaleted problems
      const e_problemstatus = 'Escaleted';
      this.problemService.getTodaysproblemWithProblemStatus(month, today_end, Mid, e_problemstatus).subscribe(eproblems => {
        this.problems = eproblems;
        this.e_m_length = this.problems.length;
        // console.log('Last Month Escaleted problems', this.problems);
      });

      // Close problems
      const c_problemstatus = 'Close';
      this.problemService.getTodaysproblemWithProblemStatus(month, today_end, Mid, c_problemstatus).subscribe(cproblems => {
        this.problems = cproblems;
        this.c_m_length = this.problems.length;
        // console.log('Last Month Close problems', this.problems);
      });
    });

    // Problems registered in six months
    this.problemService.getTodaysproblem(sixmonth, today_end, Mid).subscribe(problems => {
      this.problems = problems;
      this.length_sixmonths = this.problems.length;
      // console.log('Last six month problems:', this.problems);

      // Registered problems
      const r_problemstatus = 'Registered';
      this.problemService.getTodaysproblemWithProblemStatus(sixmonth, today_end, Mid, r_problemstatus).subscribe(rproblems => {
        this.problems = rproblems;
        this.r_sm_length = this.problems.length;
        // console.log('Last six month registered problems', this.problems);
      });

      // Waiting for approval problems
      const w_problemstatus = 'Waiting_for_Approval';
      this.problemService.getTodaysproblemWithProblemStatus(sixmonth, today_end, Mid, w_problemstatus).subscribe(wproblems => {
        this.problems = wproblems;
        this.wa_sm_length = this.problems.length;
        // console.log('Last six month Waiting for approval problems', this.problems);
      });

      // Work in progress problems
      const wp_problemstatus = 'Work_in_Progress';
      this.problemService.getTodaysproblemWithProblemStatus(sixmonth, today_end, Mid, wp_problemstatus).subscribe(wpproblems => {
        this.problems = wpproblems;
        this.wp_sm_length = this.problems.length;
        // console.log('Last six month Waiting for approval problems', this.problems);
      });

      // Escaleted problems
      const e_problemstatus = 'Escaleted';
      this.problemService.getTodaysproblemWithProblemStatus(sixmonth, today_end, Mid, e_problemstatus).subscribe(eproblems => {
        this.problems = eproblems;
        this.e_sm_length = this.problems.length;
        // console.log('Today Waiting for approval problems', this.problems);
      });

      // Closed problems
      const c_problemstatus = 'Close';
      this.problemService.getTodaysproblemWithProblemStatus(sixmonth, today_end, Mid, c_problemstatus).subscribe(cproblems => {
        this.problems = cproblems;
        this.c_sm_length = this.problems.length;
        // console.log('Today Waiting for approval problems', this.problems);
      });
    });
  }

  dashboard() {
    this.router.navigate(['/managerdashboard']);
  }

  listproblem() {
    this.router.navigate(['/managerlistproblem']);
  }

  details() {
    this.router.navigate(['/managerproblemdetail']);
  }
}
