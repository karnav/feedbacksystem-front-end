import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProblemregisterComponent } from './problemregister.component';

describe('ProblemregisterComponent', () => {
  let component: ProblemregisterComponent;
  let fixture: ComponentFixture<ProblemregisterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProblemregisterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProblemregisterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
