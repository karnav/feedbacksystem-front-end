import { Component, OnInit } from '@angular/core';

import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar, MatDatepickerModule } from '@angular/material'
import { ProblemDetail, CallType, Type, OEM, Model_no, Serial_no } from '../shared/problemdetails';
import {User} from '../shared/user';
import { Mail } from '../shared/mail';
import { ProblemService } from '../services/problem.service';
import { AuthService } from '../services/auth.service';
import { OemService } from '../services/oem.service';
import { Router } from '@angular/router';



@Component({
  selector: 'app-problemregister',
  templateUrl: './problemregister.component.html',
  styleUrls: ['./problemregister.component.scss']
})
export class ProblemregisterComponent implements OnInit {

  problemregisterform: FormGroup;
  problemdetail: ProblemDetail;
  user: User;
  errMess: string;
  //productdetail: ProductDetail;
  calltype = CallType;
  device_type = Type;
  oems: OEM[];
  Oem = [];
  oemError = new FormControl(Validators.required);

  model_no = Model_no;
  serial_no = Serial_no;
  mail = new Mail();
  problems: ProblemDetail[];
  count: string;
  protected caseid: string;
  loc_code: string;       //location code based on city

  formErrors = {
    'calltype': '',
    // 'customer_name': '',
    // 'customer_company_name': '',
    // 'contact_no': '',
    // 'email_id': '',
    // 'pincode': '',
    'device_type': '',
    'oem': '',
    'model_no': '',
    'part_no': '',
    'serial_no': '',
    'problem_title': '',
    'problem_description': ''
  }

  validationMessages = {
    'calltype': {
      'required': 'Please select Call Type.'
    },
    // 'customer_name': {
    //   'required': 'Please enter Customer Name.'
    // },
    // 'customer_company_name': {
    //   'required': 'Please enter Customer Company Name.'
    // },
    // 'contact_no': {
    //   'required': 'Please enter Mobile Number.',
    //   'pattern': 'Mobile Number must contain only numbers.',
    //   'minlength': 'Mobile Number must contain atleast 10 digits.',
    //   'maxlength': 'Mobile Number should not exceed more than 10 digits.'
    // },
    // 'email_id': {
    //   'required': 'Email Id is required.',
    //   'email': 'Email is not in valid format.'
    // },
    // 'pincode': {
    //   'required': 'Please enter pincode.',
    //   'pattern': 'Pincode must contain only numbers.',
    //   'minlength': 'Pincode must contain atleast 6 digits.',
    //   'maxlength': 'Pincode should not exceed more than 6 digits.'
    // },
    'device_type': {
      'required': 'Please select Device type.'
    },
    'oem': {
      'required': 'Please select OEM.'
    },
    'model_no': {
      'required': 'Please select Model Number.'
    },
    'part_no': {
      'required': 'Please select Part Number.'
    },
    'serial_no': {
      'required': 'Please select Serial Number.'
    },
    'problem_title': {
      'required': 'Please enter Problem Title'
    },
    'problem_description': {
      'required': 'Please enter Problem Description'
    }
  }


  constructor(private fb: FormBuilder,
    private problemService: ProblemService,
    private authService: AuthService,
    private oemService: OemService,
    private router: Router,
    public snackBar: MatSnackBar) {
    this.createForm();
  }

  ngOnInit() {
    this.authService.loadUserCredentials();
    const id = localStorage.getItem('Id');
    const Id = id.substr(1);
    const ID = Id.slice(0, -1);
    this.authService.getUserById(ID).subscribe(user => {
      this.user = user;
      console.log('user', this.user);
      // if ( this.user.company_city === 'Mumbai') {
      //   this.loc_code = '01';
      // }
    });

    this.oemService.getOems().subscribe(oems => {
      this.oems = oems;
      console.log('oems', this.oems);
      for(let i = 0; i < oems.length; i++ ) {
        this.Oem[i] = {value: this.oems[i].name};
      }
    },
    errmess => {
      this.errMess = <any>errmess;
    });
  }

  createForm() {
    this.problemregisterform = this.fb.group({
      calltype: 'Email',
      // customer_name: /*[*/''/*, Validators.required]*/,
      // customer_company_name: ''/*, Validators.required]*/,
      // contact_no: ''/*, Validators.required, Validators.pattern, Validators.minLength(10), Validators.maxLength(10)]*/,
      // email_id: ['', Validators.required, Validators.email],
      // pincode: ''/*, Validators.required, Validators.pattern, Validators.minLength(6), Validators.maxLength(6)]*/,
      device_type: 'Scanner',
      oem: '',
      model_no: 'a',
      part_no: '',
      serial_no: 'a',
      problem_title: ['', Validators.required],
      problem_description: ['', Validators.required]
    });
    this.problemregisterform.valueChanges
      .subscribe(data => this.onValueChanged(data));

    this.onValueChanged(); // (re)set validation messages now
  }

  onValueChanged(data?: any) {
    if (!this.problemregisterform) { return; }
    const form = this.problemregisterform;
    for (const field in this.formErrors) {
      // clear previous error message (if any)
      this.formErrors[field] = '';
      const control = form.get(field);
      if (control && control.dirty && !control.valid) {
        const messages = this.validationMessages[field];
        for (const key in control.errors) {
          this.formErrors[field] += messages[key] + ' ';
        }
      }
    }
  }

  onSubmit() {
    // let problem = new ProblemDetail();
    this.problemdetail = this.problemregisterform.value;
    // console.log(this.problemdetail);
    const time = new Date().toISOString();
    console.log(time);
    const t = time.slice(0, -14);
    const tn = t.substr(2);
    const ts = tn.slice(0, 2) + tn.slice(3);
    const date = ts.slice(0, 4) + ts.slice(5);
  
    const daystart = new Date();
    daystart.setHours(0, 0, 0, 0);
    const Daystart = daystart.toISOString();
    console.log(Daystart);
    const dayend = new Date();
    dayend.setHours(23, 59, 59, 999);
    const Dayend = dayend.toISOString();
    console.log(Dayend);

    // var count = '';

    this.problemService.getProblemsbyDateOnly(Daystart, Dayend).subscribe(problems => {
      this.problems = problems;
      console.log('problems', this.problems);
      if ( problems.length < 10 ) {
        const a = problems.length + 1; 
        this.count = '00' + a.toString();
        console.log(this.count);
      } else if ( problems.length < 100 ) {
        const a = problems.length + 1; 
        this.count = '0' + a.toString();
      } else if ( problems.length < 1000) {
        const a = problems.length + 1; 
        this.count = a.toString();
      }

      if ( this.user.company_city === 'Mumbai' ) {
        this.loc_code = '01';
      } else if ( this.user.company_city === 'Delhi' ) {
        this.loc_code = '02';
      } else if ( this.user.company_city === 'Chennai' ) {
        this.loc_code = '03';
      } else if ( this.user.company_city === 'Pune' ) {
        this.loc_code = '04';
      } else if ( this.user.company_city === 'Vadodara' ) {
        this.loc_code = '05';
      } else if ( this.user.company_city === 'Bangalore' ) {
        this.loc_code = '06';
      }
      this.caseid = date + this.loc_code + this.count;
      console.log('caseid', this.caseid);
      // problem.caseId = caseid;
      // console.log('before submitting', problem);
    });
    console.log('count', this.count);
    
    
    this.problemService.registerProblem(this.problemdetail).subscribe(res =>{
      // console.log(res);
      this.problemdetail = res;
      var id: string = res.customer;
      var tempId = id.substr(0);
      // console.log(tempId);
      //var Id = tempId.slice(0, -1);
      //console.log(Id);
      //For getting user Id to send mail
      this.authService.getUserById(tempId).subscribe(user => {
        this.user = user;
        console.log('user', this.user);
        if(user) {
          this.mail.subject = 'Problem registration';
          this.mail.message = 'Hello ' + this.user.username + ', we are happy to resolve your problem. Soon our support engineer will take your problem into consideration. Stay tuned!';
          //For sending mail
          this.problemService.sendMail(this.user, this.mail).subscribe(data => {
            // console.log("Mail", data);
            this.snackBar.open("Problem registered successfullly!", "Ok", {
              duration: 3000
            });
          },
          error => {
            // console.log(error.status, error.message);
            this.snackBar.open("Failed to register problem", "Ok", {
              duration: 3000
            });
          });
        }
      },
      errmess => {
        this.errMess = <any>errmess;
        this.snackBar.open(this.errMess, 'Ok', {
          duration: 4000
        });
      });
      console.log('caseiddd', this.caseid);
      let problemdetail = new ProblemDetail();
      problemdetail.caseId = this.caseid;
      this.problemService.updatecaseId(this.problemdetail._id, problemdetail).subscribe(updatedproblem => {
        this.problemdetail = updatedproblem;
        console.log('updatedproblem', this.problemdetail);
      },
      errmess => {
        this.errMess = <any>errmess;
      });

      this.problemregisterform.reset({
        calltype: 'Email',
        device_type: 'Scanner',
        oem: 'a',
        serial_no: 'a',
        model_no: 'a'
      });
    }, 
    error => {
      //For sending mail to customer
      // if(!error)
      // {
      //   this.problemService.sendMail(this.problemdetail).subscribe(res => {
      //     console.log(res);
      //   },
      //   error => {
      //     console.log(error.ststus, error.message);
      //   });
      // }
      // console.log(error.status, error.message);
      this.snackBar.open("Failed to register problem", "Ok", {
        duration: 3000
      });
    });
  }

  profile() {
    this.router.navigate(['/profile']);
  }

  registerProblem() {
    this.router.navigate(['/problemregister']);
  }

  dashboard() {
    this.router.navigate(['/userdashboard']);
  }

}
