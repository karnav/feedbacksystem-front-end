import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';

import { MatAccordion, MatExpansionPanel, MatExpansionPanelHeader,
  MatExpansionPanelActionRow, MatAccordionDisplayMode, MatSnackBar} from '@angular/material';

import { AuthService } from '../services/auth.service';
import { User, company_city } from '../shared/user';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.scss']
})
export class UserProfileComponent implements OnInit {

  user: User;
  errMess: string;
  profileForm: FormGroup;
  disabled: boolean = true;
  id: string;

  Username: string;
  EmailId: string;
  Contactno: string;
  CompanyName: string;
  CompanyAddress: string;
  CompanyCity: string;
  CompanyPincode: string;
  CompanyState: string;
  CompanyCountry: string;
  NewPassword: string;
  ConfirmPassword: string;

  usernameError = false;
  usernameErrorName: string;
  emailidError: boolean;
  emailidErrorName: string;
  contactnoError = new FormControl('', [Validators.required, Validators.maxLength(10), Validators.minLength(10),
    Validators.pattern('[0-9]*')]);
  iscontactnoError = false;
  contactnoErrorName: string;
  passwordError = false;
  passwordErrorName: string;
  companyPincodeError = new FormControl('', [Validators.required, Validators.maxLength(6), Validators.minLength(6),
     Validators.pattern('[0-9]*')]);
  isCompanyPincodeError = false;
  companyCityError = new FormControl('', [Validators.required]);

  cities = company_city;

  // formErrors = {
  //   'email_id': '',
  //   'contact_no': '',
  //   'company_name': '',
  //   'company_pincode': ''
  // };

  // validationMessages = {
  //   'email_id': {
  //     'required': 'Kindly fill Email id.',
  //     'email': 'Email id is not in valid format.'
  //   },
  //   'contact_no': {
  //     'required': 'Kindly fill Mobile number.',
  //     'pattern': 'Mobile Number must contain only numbers.',
  //     'minlength': 'Mobile Number must be of atleast 10 digits.',
  //     'maxlength': 'Mobile Number must not exceed more than 10 digits.'
  //   },
  //   'company_name': {
  //     'required': 'Kindly fill Company name.'
  //   },
  //   'company_pincode': {
  //     'pattern': 'Pincode must contain only numbers.',
  //     'minlength': 'Pincode must be of atleast 6 digits.',
  //     'maxlength': 'Pincode must not exceed more than 6 digits.'
  //   },

  // };

  constructor(
    private authService: AuthService,
    private router: Router,
    private fb: FormBuilder,
    private snackBar: MatSnackBar
  ) { 
    // this.createForm();
  }

  ngOnInit() {
    const Id = localStorage.getItem('Id');
    const tempId = Id.substr(1);
    this.id = tempId.slice(0, -1);
    // console.log(Id);
    this.authService.getUserById(this.id).subscribe(user => {
      this.user = user;
      // console.log('user', this.user);
    },
    errmess => {
      this.errMess = errmess;
      this.snackBar.open(this.errMess, 'Ok', {
        duration: 4000
      });
    });
  }

  profile() {
    this.router.navigate(['/profile']);
  }

  registerProblem() {
    this.router.navigate(['/problemregister']);
  }

  dashboard() {
    this.router.navigate(['/userdashboard']);
  }

  // createForm() {
  //   this.profileForm = this.fb.group({
  //     // firstname: {value: '', disabled: false},
  //     // lastname: {value: '', disabled: false},
  //     email_id: [{value: '', disabled: false}, [Validators.email, Validators.required]],
  //     contact_no: [{value: '', disabled: false}, [Validators.pattern, Validators.minLength(10), Validators.maxLength(10), Validators.required]],
  //     company_name: [{value: '', disabled: false}, Validators.required],
  //     company_address: {value: '', disabled: false},
  //     company_city: {value: '', disabled: false},
  //     company_pincode: [{value: '', disabled: false}, [Validators.pattern, Validators.minLength(6), Validators.maxLength(6)]],
  //     company_state: {value: '', disabled: false},
  //     company_country: {value: '', disabled: false}
  //   });

  //   this.profileForm.valueChanges.subscribe(data => this.onValueChanged(data));
  //   this.onValueChanged();

  // }


  // onSubmit() {
  //   this.user = this.profileForm.value;
  //   console.log(this.user);
  //   var id = localStorage.getItem('Id');
  //   var tempid = id.substr(1);
  //   var Id = tempid.slice(0, -1);
  //   console.log(id)
  //   console.log('userprofile', this.user);
  //   this.authService.updateUser(Id, this.user).subscribe(user => {
  //     this.user = user;
  //   },
  //   errmess => {
  //     this.errMess = <any>errmess;
  //   });
  // }

  // onValueChanged(data?: any) {
  //   if (!this.profileForm) { return; }
  //   const form = this.profileForm;
  //   for (const field in this.formErrors) {
      // clear previous error message (if any)
  //     this.formErrors[field] = '';
  //     const control = form.get(field);
  //     if (control && control.dirty && !control.valid) {
  //       const messages = this.validationMessages[field];
  //       for (const key in control.errors) {
  //         this.formErrors[field] += messages[key] + ' ';
  //       }
  //     }
  //   }
  // }

  onSubmitUsername() {
    if ( this.Username.length < 2 ) {
      this.usernameError = true;
      this.usernameErrorName = 'Username must contain atleast 2 characters';
    } else if ( this.Username.length > 2) {
      this.usernameError = false;
    }
    if ( this.usernameError === false) {
      this.user.username = this.Username.trim();
      this.authService.updateUser(this.id, this.user ).subscribe(user => {
        this.user = user;
        this.snackBar.open('Username changed successfully!', 'Ok', {
          duration: 3000
        });
      },
      errmess => {
        this.errMess = <any>errmess;
        this.snackBar.open(this.errMess, 'Ok', {
          duration: 4000
        });
      });
    }
  }

  onSubmitPassword() {
    if ((this.NewPassword.length >= 8 && this.NewPassword.length <= 20) && (this.NewPassword === this.ConfirmPassword) ) {
      this.passwordError = false;
      this.user.password = this.NewPassword.trim();
      this.authService.updateUser(this.id, this.user ).subscribe(user => {
        this.user = user;
        this.snackBar.open('Password changed successfully!', 'Ok', {
          duration: 3000
        });
      },
      errmess => {
        this.errMess = <any>errmess;
        this.snackBar.open(this.errMess, 'Ok', {
          duration: 4000
        });
      });
    } else {
      this.passwordError = true;
      this.passwordErrorName = 'New password and confirm password didnt match';
    }
  }

  onSubmitEmailId() {
    this.user.email_id = this.EmailId.trim();
    this.authService.updateUser(this.id, this.user).subscribe(user => {
      this.user = user;
      this.snackBar.open('Email id changed successfully!', 'Ok', {
        duration: 3000
      });
    },
    errmess => {
      this.errMess = <any>errmess;
      this.snackBar.open(this.errMess, 'Ok', {
        duration: 4000
      });
    });
  }

  onSubmitContactno() {
    if( this.Contactno.length > 10 || this.Contactno.length < 10) {
      this.iscontactnoError = true;
    } else {
      this.iscontactnoError = false;
      this.user.contact_no = this.Contactno;
      this.authService.updateUser(this.id, this.user).subscribe(user => {
      this.user = user;
      this.snackBar.open('Contact Number changed successfully!', 'Ok', {
        duration: 3000
      });
    },
    errmess => {
      this.errMess = <any>errmess;
      this.snackBar.open(this.errMess, 'Ok', {
        duration: 4000
      });
    });
  }
  }

  onSubmitCompanyName() {
    this.user.company_name = this.CompanyName.trim();
    this.authService.updateUser(this.id, this.user).subscribe(user => {
      this.user = user;
      this.snackBar.open('Company name changed successfully!', 'Ok', {
        duration: 3000
      });
    },
    errmess => {
      this.errMess = <any>errmess;
      this.snackBar.open(this.errMess, 'Ok', {
        duration: 4000
      });
    });
  }

  onSubmitCompanyAddress() {
    this.user.company_address = this.CompanyAddress.trim();
    this.authService.updateUser(this.id, this.user).subscribe(user => {
      this.user = user;
      this.snackBar.open('Company street address changed successfully!', 'Ok', {
        duration: 3000
      });
    },
    errmess => {
      this.errMess = <any>errmess;
      this.snackBar.open(this.errMess, 'Ok', {
        duration: 4000
      });
    });
  }

  onSubmitCompanyCity() {
    console.log('city', this.CompanyCity);
    this.user.company_city = this.CompanyCity;
    this.authService.updateUser(this.id, this.user).subscribe(user => {
      this.user = user;
      this.snackBar.open('City changed successfully!', 'Ok', {
        duration: 3000
      });
    },
    errmess => {
      this.errMess = <any>errmess;
      this.snackBar.open(this.errMess, 'Ok', {
        duration: 4000
      });
    });
  }

  onSubmitPincode() {
    if ( this.CompanyPincode.length > 6 || this.CompanyPincode.length < 6) {
      this.isCompanyPincodeError = true;
    } else {
      this.isCompanyPincodeError = false;
      this.user.company_pincode = this.CompanyPincode;
      this.authService.updateUser(this.id, this.user).subscribe(user => {
        this.user = user;
        this.snackBar.open('Pincode changed successfully!', 'Ok', {
          duration: 3000
        });
      },
      errmess => {
        this.errMess = <any>errmess;
        this.snackBar.open(this.errMess, 'Ok', {
          duration: 4000
        });
      });
    }
  }

  onSubmitState() {
    this.user.company_state = this.CompanyState.trim();
    this.authService.updateUser(this.id, this.user).subscribe(user => {
      this.user = user;
      this.snackBar.open('State changed successfully!', 'Ok', {
        duration: 3000
      });
    },
    errmess => {
      this.errMess = <any>errmess;
      this.snackBar.open(this.errMess, 'Ok', {
        duration: 4000
      });
    });
  }

  onSubmitCountry() {
    this.user.company_country = this.CompanyCountry.trim();
    this.authService.updateUser(this.id, this.user).subscribe(user => {
      this.user = user;
      this.snackBar.open('Country changed successfully!', 'Ok', {
        duration: 3000
      });
    },
    errmess => {
      this.errMess = <any>errmess;
      this.snackBar.open(this.errMess, 'Ok', {
        duration: 4000
      });
    });
  }


}
