import { Component, OnInit, ViewChild } from '@angular/core';
import { MatSnackBar, MatPaginator, MatTable, MatTableDataSource, MatSort, MatDialog, MatDialogRef } from '@angular/material';

import { Router } from '@angular/router';

import { ProblemService } from '../services/problem.service';

import { ProblemDetail } from '../shared/problemdetails';
import { ProblemTransaction } from '../shared/problemtransactions';
import { User } from '../shared/user';

@Component({
  selector: 'app-worklog',
  templateUrl: './worklog.component.html',
  styleUrls: ['./worklog.component.scss']
})
export class WorklogComponent implements OnInit {

  problems: ProblemDetail[];
  problemtransactions: ProblemTransaction[];
  users: User[];
  startDate: Date;
  endDate: Date;
  errMess: string;

  // paginator input
  length: number;

  // table columns
  displayedColumns = ['createdAt', 'problem_title', 'problem_status', 'customer_name', 'engineer', 'updatedAt'];
  dataSource: MatTableDataSource<ProblemDetail>;

  constructor(
    private router: Router,
    private problemService: ProblemService,
    private snackBar: MatSnackBar
  ) { }

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  ngOnInit() {
  }

  dashboard() {
    this.router.navigate(['/sengdashboard']);
  }

  worklog() {
    this.router.navigate(['/worklog']);
  }

  onSubmit() {
    // var startdate =this.startDate;
    const startdate = new Date(this.startDate);
    const sdate = startdate.toISOString();
    const Id = localStorage.getItem('Id');
    const tid = Id.substr(1);
    const id = tid.slice(0, -1);
    // console.log('startdate', sdate);

    // var enddate = this.endDate;
    const enddate = new Date(this.endDate);
    const edate = enddate.toISOString();
    // console.log(startdate);
    // console.log(enddate);
    // console.log('enddate', edate);
    //  this.query.query_string= '{updatedAt:{$gte:\'' + sdate  + '\',$lte:\'' + sdate + '\'},engineer: \'' + id + '\'}';
    // start editing from here
    // var query = '?{"updatedAt":{$gte:new ISODate("'+sdate+'"),$lte:new ISODate("'+edate+'")}}';
    // this.query.query_string = this.Query;
    // console.log('query class', this.query);
    const problemstatus = '';
    this.problemService.getProblemsbyDate(sdate, edate, id, problemstatus).subscribe( problems => {
      this.problems = problems;
      // console.log('Problems:', problems);
      this.dataSource = new MatTableDataSource(this.problems);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
      this.length = this.problems.length;
    },
    errmess => {
      this.errMess = <any>errmess;
      this.snackBar.open(this.errMess, 'Ok', {
        duration: 4000
      });
    });

  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

}
