import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManagerlistproblemComponent } from './managerlistproblem.component';

describe('ManagerlistproblemComponent', () => {
  let component: ManagerlistproblemComponent;
  let fixture: ComponentFixture<ManagerlistproblemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManagerlistproblemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManagerlistproblemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
