import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';

import {ProblemDetail, ProblemStatus} from '../shared/problemdetails';
import { ProblemService } from '../services/problem.service';
import { AuthService } from '../services/auth.service';

import { Params, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { MatSnackBar, MatPaginator, MatTable, MatTableDataSource, MatSort, MatDialog, MatDialogRef } from '@angular/material';

import { User } from '../shared/user';
import { Mail } from '../shared/mail';

@Component({
  selector: 'app-managerlistproblem',
  templateUrl: './managerlistproblem.component.html',
  styleUrls: ['./managerlistproblem.component.scss']
})
export class ManagerlistproblemComponent implements OnInit {

  problems: ProblemDetail[];
  problem: ProblemDetail;
  startDate: Date;
  endDate: Date;
  errMess: string;
  Engineers = [];
  selectedValue: string; // For selecting problem status in report div
  editCardId: any = '';
  selectedValue1 = 'Registered'; // For selecting problem status in problem card
  problemStatus = ProblemStatus;
  Eid: string;
  user: User;
  mail = new Mail();
  eng: string;
  engusername: string;
  problemCreatedDate: Date;

  // paginator input
  length: number;

  // table columns
  displayedColumns = ['createdAt', 'problem_title', 'problem_status', 'customer_name', 'engineer', 'updatedAt', 'actions'];
  dataSource: MatTableDataSource<ProblemDetail>;

  constructor(
    private route: ActivatedRoute,
    private location: Location,
    private problemService: ProblemService,
    private router: Router,
    private snackBar: MatSnackBar,
    private authService: AuthService
  ) { }

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  ngOnInit() {
    // to get details of manager
    this.eng = localStorage.getItem('Id');
    const eid = this.eng.substr(1);
    const EID = eid.slice(0, -1);
    this.authService.getUserById(EID).subscribe(user => {
      this.user = user;
      this.engusername = this.user.firstname + ' ' + this.user.lastname;
    },
    errmess => {
      this.errMess = <any>errmess;
    });
  }

  onSubmit() {
    // var startdate =this.startDate;
    const startdate = new Date(this.startDate);
    const sdate = startdate.toISOString();
    // console.log('startdate',sdate);

    //  const Id = this.selectedValue;
    //  console.log(Id);
    // const tid = Id.substr(1);
    // const id = tid.slice(0, -1);
    

    // var enddate = this.endDate;
    const enddate = new Date(this.endDate);
    const edate = enddate.toISOString();
    // console.log(startdate);
    // console.log(enddate);
    // console.log('enddate', edate);
    
    


    const id = '';
    const problemstatus = this.selectedValue;
    this.problemService.getProblemsbyDate(sdate, edate, id, problemstatus).subscribe( problems => {
      this.problems = problems;
      console.log('Problems:', problems);
      this.dataSource = new MatTableDataSource(this.problems);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
      this.length = this.problems.length;
      // console.log('length', length);
     
    },
    errmess => {
      this.errMess = <any>errmess;
    });

  }

  onEdit(id: string) {
    //this.editable = !this.editable ;
    this.editCardId = id;
    // console.log(this.editCardId);
  }

  onSave(id: string) {
    // console.log('probId', id);
    const data: string = this.selectedValue1;
    this.problem = new ProblemDetail();
    this.problem.problem_status = data;
    // console.log('problem', this.problem);
    const startdate = new Date(this.startDate);
    const sdate = startdate.toISOString();
    // console.log(startdate);
    const enddate = new Date(this.endDate);
    const edate = enddate.toISOString();
    // console.log(startdate);
    // const Id = this.selectedValue;
    // console.log(Id);

    // Manager Id
    const Mid = localStorage.getItem('Id');
    // console.log('Eng Id', Mid);
    const mid = Mid.substr(1);
    const MID = mid.slice(0, -1);

    this.problemService.updateProblem(id, this.problem, this.Eid).subscribe( problem => {
      this.problem = problem;
      // console.log('Updated problem: ', problem);

      //customer Id 
      const Cid = this.problem.customer;
      // console.log('Customer Id', Cid);      
      // console.log(problem);
      this.problemCreatedDate = new Date(this.problem.createdAt);
      const year = this.problemCreatedDate.getFullYear();
      const month = this.problemCreatedDate.getMonth() + 1;
      const day = this.problemCreatedDate.getDate();
      const date = day + '/' + month + '/' + year;

      // geting user for sending mail
      this.authService.getUserById(Cid).subscribe(user => {
        this.user = user;
        // console.log(this.user);
        if(user) {
          this.mail.subject = 'Problem status updated';
          this.mail.message = 'Hello ' + this.user.username + ', your problem having title \'' + this.problem.problem_title + '\' which was registered on \'' + date + '\' has been taken by our manager. Our manager named \'' + this.engusername + '\' has set status of your problem to \'' + this.problem.problem_status + '\'!';
          //For sending mail
          this.problemService.sendMail(this.user, this.mail).subscribe(data => {
            // console.log("Mail", data);
            this.snackBar.open("Mail sent successfullly!", "Ok", {
              duration: 3000
            });
          },
          error => {
            // console.log(error.status, error.message);
            this.snackBar.open("Failed to send mail problem", "Ok", {
              duration: 3000
            });
          });
        }
      },
      errmess => {
        this.errMess = <any>errmess;
      });

      id = '';
      this.editCardId = '';
      // console.log('id', id);
      this.snackBar.open("Problem edited successfully!", "Ok", {
        duration: 2000
      });

      const problemstatus = this.selectedValue;
      const Id = '';
      this.problemService.getProblemsbyDate(sdate, edate, Id, problemstatus).subscribe( problems => {
        this.problems = problems;
        // console.log('Problems:', problems);
      },
      errmess => {
        this.errMess = <any>errmess;
      });
    },
    errmess => {
      this.errMess = <any>errmess;
      this.snackBar.open("Something went wrong!", "Ok", {
        duration: 2000
      });
    });
  }

  onCancel() {
    
    this.editCardId = null;
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

  dashboard() {
    this.router.navigate(['/managerdashboard']);
  }

  listproblem() {
    this.router.navigate(['/managerlistproblem']);
  }


}
