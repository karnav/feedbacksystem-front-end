import { Injectable } from '@angular/core';

import { ProblemDetail } from '../shared/problemdetails';
import { ProblemTransaction } from '../shared/problemtransactions';
import { User } from '../shared/user';
import { Query } from '../shared/query';
import { baseURL } from '../shared/baseurl';

import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Http, Headers } from '@angular/http';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { ProcessHttpmsgService } from './process-httpmsg.service';
import { AuthService } from './auth.service';

import 'rxjs/add/operator/delay';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import { RequestOptions } from '@angular/http/src/base_request_options';
import { startTimeRange } from '@angular/core/src/profile/wtf_impl';
import { startWith } from 'rxjs/operator/startWith';
import { query } from '@angular/core/src/animation/dsl';
import { Mail } from '../shared/mail';

@Injectable()
export class ProblemService {

  problemdetail: ProblemDetail;
  user: User;

  constructor(private http: HttpClient,
    private _http: Http,
    private processHTTPMsgService: ProcessHttpmsgService,
    //private headers: Headers,
    private authService: AuthService) { }

  registerProblem(problemdetail: ProblemDetail): Observable<ProblemDetail> {
    
    console.log('inserviceproblem', problemdetail);
    const authToken = this.authService.getToken();
    //trim first character
    let myString = authToken.substr(1);   // Need to trim first and last character of authToken because it stores in the for ""abc"" which results  in mismatch of token
    //trim last character
    var actual = myString.slice(0, -1);  
    // console.log(authToken);
    let headers = new Headers();
    headers.append('Authorization', 'bearer ' + actual);
    // console.log(headers);

    return this._http.post(baseURL + 'problems', problemdetail, {headers: headers})
    .map(res => res.json())
    .catch(error => {
      return this.processHTTPMsgService.handleError(error);
    });    
  }

  //Sending mail to customer after successfully registering problem
  sendMail(user: User, mail: Mail): Observable<User> {

    const authToken = this.authService.getToken();
    //trim first character
    var myString = authToken.substr(1);   // Need to trim first and last character of authToken because it stores in the for ""abc"" which results  in mismatch of token
    //trim last character
    var actual = myString.slice(0, -1);  
    // console.log(authToken);
    let headers = new Headers();
    headers.append('Authorization', 'bearer ' + actual);
    // console.log(headers);

    let params = new HttpParams();
    params.set('subject', mail.subject);
    params.set('message', mail.message);

    // console.log("problemservice user", user);
    return this._http.post(baseURL + 'mail?subject=' + mail.subject + '&message=' + mail.message, user, {headers: headers, params: params})
    .map(res => res.json())
    .catch(error => {
      return this.processHTTPMsgService.handleError(error);
    });
    
  }

  //Get problemdetails whose problem_status is waiting 

  getRegisteredProblems(): Observable<any> {

    const authToken = this.authService.getToken();
    //trim first character
    var myString = authToken.substr(1);   // Need to trim first and last character of authToken because it stores in the for ""abc"" which results  in mismatch of token
    //trim last character
    var actual = myString.slice(0, -1);  
    // console.log(authToken);
    let headers = new Headers();
    headers.append('Authorization', 'bearer ' + actual);
    // console.log(headers);
    //var query = 
    //return this._http.get("http://localhost:3000/problems?problem_status="+data, {headers: headers})
    return this._http.get(baseURL + 'problems?problem_status=Registered', {headers: headers})
    .map(res => res.json())
    .catch(error => {
      return this.processHTTPMsgService.handleError(error);
    });
  }

  getProblems(): Observable<ProblemDetail[]> {
    const authToken = this.authService.getToken();
    //trim first character
    var myString = authToken.substr(1);   // Need to trim first and last character of authToken because it stores in the for ""abc"" which results  in mismatch of token
    //trim last character
    var actual = myString.slice(0, -1);  
    // console.log(authToken);
    let headers = new Headers();
    headers.append('Authorization', 'bearer ' + actual);
    // console.log(headers);

    return  this._http.get(baseURL + 'problems', {headers: headers} )
    .map(res => res.json())
    .catch(error => {
      return this.processHTTPMsgService.handleError(error);
    });
  }

  //to get problems on base of problem_status
  getSelectedProblems(data: string): Observable<ProblemDetail[]> {
    const authToken = this.authService.getToken();
    //trim first character
    var myString = authToken.substr(1);   // Need to trim first and last character of authToken because it stores in the for ""abc"" which results  in mismatch of token
    //trim last character
    var actual = myString.slice(0, -1);  
    // console.log(authToken);
    let headers = new Headers();
    headers.append('Authorization', 'bearer ' + actual);
    // console.log(headers);

    return  this._http.get(baseURL + 'problems?problem_status='+data, {headers: headers} )
    .map(res => res.json())
    .catch(error => {
      return this.processHTTPMsgService.handleError(error);
    });
  }

  //to get problems of a specific user
  getUserProblems(id:string): Observable<ProblemDetail[]> {
    const authToken = this.authService.getToken();
    //trim first character
    var myString = authToken.substr(1);   // Need to trim first and last character of authToken because it stores in the for ""abc"" which results  in mismatch of token
    //trim last character
    var actual = myString.slice(0, -1);  

    //trim Id
    var tempId = id.substr(1);
    var Id = tempId.slice(0, -1);

    // console.log(authToken);
    let headers = new Headers();
    headers.append('Authorization', 'bearer ' + actual);
    // console.log(headers);

    return this._http.get(baseURL + 'problems?customer='+Id, {headers: headers})
    .map(res => res.json())
    .catch( error => {
      return this.processHTTPMsgService.handleError(error);
    });
  }


  //get a single problemdetail from _id
  getProblem(id: string): Observable<ProblemDetail> {
    const authToken = this.authService.getToken();
    //trim first character
    var myString = authToken.substr(1);   // Need to trim first and last character of authToken because it stores in the for ""abc"" which results  in mismatch of token
    //trim last character
    var actual = myString.slice(0, -1);  
    // console.log(authToken);
    let headers = new Headers();
    headers.append('Authorization', 'bearer ' + actual);
    // console.log(headers);
    // console.log(id);
    //var _id = id.substr(17);
    return this._http.get(baseURL + 'problems/' + id, {headers: headers} )
    .map(res => res.json())
    .catch(error => {
      return this.processHTTPMsgService.handleError(error);
    });
  }

  getProblemIds(): Observable<string[] | any> {
    return this.getProblems().
      map(problems => problems.map(problem => problem._id));
  }

  // get problems by date
  getProblemsbyDate(sdate, edate, id, problemstatus: string): Observable<ProblemDetail[]> {
    const authToken = this.authService.getToken();
    // trim first character
    const myString = authToken.substr(1);   // Need to trim first and last character of authToken because it stores in the for ""abc"" which results  in mismatch of token
    // trim last character
    const actual = myString.slice(0, -1);  
    // console.log(authToken);
    let headers = new Headers();
    headers.append('Authorization', 'bearer ' + actual);
    // console.log(headers);

    // const id = Id.substr(1);
    // const tid = id.slice(0, -1);
    // console.log('Id', tid);
    // console.log(sdate);
    let params = new HttpParams();
    params.set('sdate', sdate);
    params.set('edate',edate);
    params.set('Eid', id);
    params.set('problemstatus', problemstatus);
    // params.set('edate', edate);
    // params.set('Id', Id);
    // console.log('params',params);
    // params.set(startTimeRange(sdate, edate), 'updatedAt');
    // params.set('end', edate);
    // params.set('engineer', Id);
    // params.append({where: {
    //      "updatedAt": { "gte": startTimeRange, "lte": edate }
    //    }})
    // let requestOptions = new RequestOptions();
    // requestOptions.params = params;
    // requestOptions.headers = headers;
    // params.set({
    //  $and: { updatedAt: { $gte: sdate, $lte: edate }}, { engineer: Id }});
    // params.append('%7B%24and%3A%7B"updatedAt"%3A%7B"gte"%3A'+sdate+'%2C"lte"%3A'+edate+'%7D%7D%2C%7B"engineer"%3A'+Id+'%7D%7D');
    // return this._http.get('http://localhost:3000/problems', {headers: headers} )
    // const query: string = 'updatedAt=%7B%24gt%3A' + sdate + '%2C%24lt=' + edate + '%7D&engineer=' + tid;
    

    const z='abc';
     // tslint:disable-next-line:max-line-length
     return this._http.post(baseURL + 'problems?Eid='+id+'&sdate='+sdate+'&edate='+edate+'&problemstatus='+problemstatus, z, {headers: headers, params: params})
    .map(res => res.json())
    .catch(error => {
      return this.processHTTPMsgService.handleError(error);
    });
  }

  updatecaseId(id: string, problem:ProblemDetail): Observable<ProblemDetail> {
    const authToken = this.authService.getToken();
    // trim first character
    var myString = authToken.substr(1);   // Need to trim first and last character of authToken because it stores in the for ""abc"" which results  in mismatch of token
    // trim last character
    var actual = myString.slice(0, -1);  
    // console.log(authToken);
    let headers = new Headers();
    headers.append('Authorization', 'bearer ' + actual);

    return this._http.put(baseURL + 'problems/' + id, problem, {headers: headers})
    .map(res => res.json())
    .catch(error => {
      return this.processHTTPMsgService.handleError(error);
    });
  }

  updateProblem(id: string, problem: ProblemDetail, Eid): Observable<ProblemDetail> {
    const authToken = this.authService.getToken();
    // trim first character
    var myString = authToken.substr(1);   // Need to trim first and last character of authToken because it stores in the for ""abc"" which results  in mismatch of token
    // trim last character
    var actual = myString.slice(0, -1);  
    // console.log(authToken);
    let headers = new Headers();
    headers.append('Authorization', 'bearer ' + actual);
    // console.log(headers);
    // console.log('Eid$', Eid);
    let params = new HttpParams();
    params.set('Eid', Eid);
    
    // console.log(JSON.stringify(problem));
    return this._http.put(baseURL + 'problems/' + id + '?Eid=' + Eid, problem, {headers: headers, params: params})
    .map(res => res.json())
    .catch(error => {
      return this.processHTTPMsgService.handleError(error);
    });

  }

  deleteProblem(id: string): Observable<ProblemDetail[]> {
    const authToken = this.authService.getToken();
    // trim first character
    var myString = authToken.substr(1);   // Need to trim first and last character of authToken because it stores in the for ""abc"" which results  in mismatch of token
    // trim last character
    var actual = myString.slice(0, -1);  
    // console.log(authToken);
    let headers = new Headers();
    headers.append('Authorization', 'bearer ' + actual);
    // console.log(headers);
    // console.log(id);

    return this._http.delete(baseURL + 'problems/' + id, {headers: headers})
    .map(res => res.json())
    .catch(error => {
      return this.processHTTPMsgService.handleError(error);
    });
  } 

  
  postProblemtransaction(problem: ProblemDetail, EID, Cid): Observable<ProblemTransaction> {

    const authToken = this.authService.getToken();
    // trim first character
    var myString = authToken.substr(1);   // Need to trim first and last character of authToken because it stores in the for ""abc"" which results  in mismatch of token
    // trim last character
    var actual = myString.slice(0, -1);
    // console.log(authToken);
    let headers = new Headers();
    headers.append('Authorization', 'bearer ' + actual);
    // console.log(headers);

    let params = new HttpParams();
    params.set('EID', EID);
    params.set('Cid', Cid);

    return this._http.post(baseURL + 'problemtransactions?EID='+EID+'&Cid='+Cid+'', problem, {headers: headers, params: params})
    .map(res => res.json())
    .catch( error => {
      return this.processHTTPMsgService.handleError(error);
    });
  }

  postProblemtransactionBySeniorEng(problem: ProblemDetail,Sid: string, EID, Cid): Observable<ProblemTransaction> {
    const authToken = this.authService.getToken();
        // trim first character
    var myString = authToken.substr(1);   // Need to trim first and last character of authToken because it stores in the for ""abc"" which results  in mismatch of token
        // trim last character
    var actual = myString.slice(0, -1);
    // console.log(authToken);
    let headers = new Headers();
    headers.append('Authorization', 'bearer ' + actual);
    // console.log(headers);

    const sid = Sid.substr(1);
    const SID = sid.slice(0, -1);
    
    let params = new HttpParams();
    params.set('EID', EID);
    params.set('Cid', Cid);
    params.set('Sid', Sid);
    
    return this._http.post(baseURL + 'problemtransactions?EID='+EID+'&Cid='+Cid+'&Sid='+SID, problem, {headers: headers, params: params})
    .map(res => res.json())
    .catch( error => {
      return this.processHTTPMsgService.handleError(error);
    });
  }

  getTodaysproblem(today_start: string, today_end: string, Mid: string): Observable<ProblemDetail[]> {
    const authToken = this.authService.getToken();
    // trim first character
    var myString = authToken.substr(1);   // Need to trim first and last character of authToken because it stores in the for ""abc"" which results  in mismatch of token
    // trim last character
    var actual = myString.slice(0, -1);
    // console.log(authToken);
    let headers = new Headers();
    headers.append('Authorization', 'bearer ' + actual);
    // console.log(headers);

    const mid = Mid.substr(1);
    const MID = mid.slice(0, -1);

    const problemstatus = '';
    
    let params = new HttpParams();
    params.set('MID', MID);
    params.set('sdate', today_start);
    params.set('edate', today_end);
    params.set('problemstatus', problemstatus);
  
    const z = 'abc';

    return this._http.post(baseURL + 'problems?MID='+MID+'&sdate='+today_start+'&edate='+today_end+'&problemstatus='+problemstatus, z, {headers: headers, params: params})
    .map(res => res.json())
    .catch( error => {
      return this.processHTTPMsgService.handleError(error);
    });
  }

  getTodaysproblemWithProblemStatus(today_start: string, today_end: string, Mid: string, problemstatus: string): Observable<ProblemDetail[]> {
    const authToken = this.authService.getToken();
    // trim first character
    var myString = authToken.substr(1);   // Need to trim first and last character of authToken because it stores in the for ""abc"" which results  in mismatch of token
    // trim last character
    var actual = myString.slice(0, -1);
    // console.log(authToken);
    let headers = new Headers();
    headers.append('Authorization', 'bearer ' + actual);
    // console.log(headers);

    const mid = Mid.substr(1);
    const MID = mid.slice(0, -1);
    
    // console.log('problemstatus', problemstatus);
    let params = new HttpParams();
    params.set('MID', MID);
    params.set('sdate', today_start);
    params.set('edate', today_end);
    params.set('problemstatus', problemstatus);
  
    const z = 'abc';

    var c: number = c + 1;
    // console.log('count', c);

    return this._http.post(baseURL + 'problems?MID='+MID+'&sdate='+today_start+'&edate='+today_end+'&problemstatus='+problemstatus, z, {headers: headers, params: params})
    .map(res => res.json())
    .catch( error => {
      return this.processHTTPMsgService.handleError(error);
    });
  }

  getProblemsbyDateOnly(daystart: string, dayend: string): Observable<ProblemDetail[]> {
    const authToken = this.authService.getToken();
    // trim first character
    var myString = authToken.substr(1);   // Need to trim first and last character of authToken because it stores in the for ""abc"" which results  in mismatch of token
    // trim last character
    var actual = myString.slice(0, -1);
    // console.log(authToken);
    let headers = new Headers();
    headers.append('Authorization', 'bearer ' + actual);
    const type = 'daterange';
    let params = new HttpParams();
    params.set('sday', daystart);
    params.set('eday', dayend);
    params.set('tYpe', type);

    return this._http.get(baseURL + 'problems?sday=' + daystart + '&eday=' + dayend + '&tYpe=' + type, {headers: headers, params: params})
    .map(res => res.json())
    .catch( error => {
      return this.processHTTPMsgService.handleError(error);
    });
  }

}
