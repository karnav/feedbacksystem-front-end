import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Http, Headers } from '@angular/http';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { User } from '../shared/user';
import { baseURL } from '../shared/baseurl';
import { OEM } from '../shared/problemdetails';

import { ProcessHttpmsgService } from './process-httpmsg.service';
import { AuthService } from './auth.service';

import 'rxjs/add/operator/delay';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

@Injectable()
export class OemService {

  constructor(
    private http: HttpClient,
    private _http: Http,
    private processHTTPMsgService: ProcessHttpmsgService,
    //private headers: Headers,
    private authService: AuthService
  ) { }

  getOems(): Observable<any[]> {
    const authToken = this.authService.getToken();
    //trim first character
    var myString = authToken.substr(1);   // Need to trim first and last character of authToken because it stores in the for ""abc"" which results  in mismatch of token
    //trim last character
    var actual = myString.slice(0, -1);  
    // console.log(authToken);
    let headers = new Headers();
    headers.append('Authorization', 'bearer ' + actual);

    return this._http.get(baseURL + 'oems', {headers: headers})
    .map(res => res.json())
    .catch(error => {
      return this.processHTTPMsgService.handleError(error);
    });
  }

  postOem(oem: OEM): Observable<OEM[]> {
    const authToken = this.authService.getToken();
    //trim first character
    var myString = authToken.substr(1);   // Need to trim first and last character of authToken because it stores in the for ""abc"" which results  in mismatch of token
    //trim last character
    var actual = myString.slice(0, -1);  
    // console.log(authToken);
    let headers = new Headers();
    headers.append('Authorization', 'bearer ' + actual);

    return this._http.post(baseURL + 'oems', oem, {headers: headers})
    .map(res => res.json())
    .catch(error => {
      return this.processHTTPMsgService.handleError(error);
    });
  }

  deleteOem(id: string): Observable<OEM> {
    const authToken = this.authService.getToken();
    //trim first character
    var myString = authToken.substr(1);   // Need to trim first and last character of authToken because it stores in the for ""abc"" which results  in mismatch of token
    //trim last character
    var actual = myString.slice(0, -1);  
    // console.log(authToken);
    let headers = new Headers();
    headers.append('Authorization', 'bearer ' + actual);

    return this._http.delete(baseURL + 'oems/' + id, {headers: headers})
    .map(res => res.json())
    .catch(error => {
      return this.processHTTPMsgService.handleError(error);
    });
  }

}
