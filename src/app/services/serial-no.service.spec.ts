import { TestBed, inject } from '@angular/core/testing';

import { SerialNoService } from './serial-no.service';

describe('SerialNoService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SerialNoService]
    });
  });

  it('should be created', inject([SerialNoService], (service: SerialNoService) => {
    expect(service).toBeTruthy();
  }));
});
