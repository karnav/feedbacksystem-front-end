import { Component, OnInit } from '@angular/core';

import {  MatDialog, MatDialogRef, MatSnackBar } from '@angular/material';
import { ProblemDetail, ProblemStatus } from '../../shared/problemdetails';
import { ProblemService } from '../../services/problem.service';

@Component({
  selector: 'app-problemdetails',
  templateUrl: './problemdetails.component.html',
  styleUrls: ['./problemdetails.component.scss']
})
export class ProblemdetailsComponent implements OnInit {

  problem: ProblemDetail;
  errMess: string;

  constructor(
    private problemService: ProblemService,
    private snackBar: MatSnackBar
  ) { }

  ngOnInit() {
    const pid = localStorage.getItem('ProblemId');
    // console.log(pid);
    this.problemService.getProblem(pid).subscribe(problem => {
      this.problem = problem;
      // console.log('yu', this.problem);
    },
    errmess => {
      this.errMess = <any>errmess;
      this.snackBar.open(this.errMess, 'Ok', {
        duration: 4000
      });
    });
    localStorage.removeItem('ProblemId');
  }

}
