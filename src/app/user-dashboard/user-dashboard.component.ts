import { Component, OnInit, ViewChild } from '@angular/core';
import { MatSidenavModule, MatTableDataSource, MatPaginator, MatSort, MatDialog, MatDialogRef, MatSnackBar } from '@angular/material';
import { Router } from '@angular/router';

import { User } from '../shared/user';
import { ProblemService } from '../services/problem.service';
import { ProblemDetail } from '../shared/problemdetails';
import { ProblemdetailsComponent } from './problemdetails/problemdetails.component';
// import { setTimeout } from 'timers';
import { AuthService } from '../services/auth.service';
import { flyInOut, expand } from '../animations/app.animations';

@Component({
  selector: 'app-user-dashboard',
  templateUrl: './user-dashboard.component.html',
  styleUrls: ['./user-dashboard.component.scss'],
  animations: [
    flyInOut(),
    expand()
  ]
})
export class UserDashboardComponent implements OnInit {

  problems: ProblemDetail[];
  user: User;
  errMess: string;
  // paginator input
  length: number;

   // table columns
   displayedColumns = ['createdAt', 'caseid', 'problem_title', 'problem_status', 'problem_details'];
   dataSource: MatTableDataSource<ProblemDetail>;

  constructor(
    private problemService: ProblemService,
    private router: Router,
    public dialog: MatDialog,
    private snackBar: MatSnackBar,
    private authService: AuthService
  ) { }

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  ngOnInit() {
    const id = localStorage.getItem('Id');
    const Id = id.substr(1);
    const ID = Id.slice(0, -1);
    // console.log(id);
    this.authService.getUserById(ID).subscribe(user => {
      this.user = user;
      // console.log(this.user);
      if ( this.user.email_id === '' ) {
        setTimeout(() => {
          this.snackBar.open('Kindly update your profile!', 'Ok', {
            duration: 3000
          });
        }, 2000);
      }
    });
    this.problemService.getUserProblems(id).subscribe(problems => {
      this.problems = problems;
      this.dataSource = new MatTableDataSource(this.problems);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
      this.length = this.problems.length;
      // console.log("problems: ", problems);
    },
    errmess => {
      this.errMess = <any>errmess;
      this.snackBar.open(this.errMess, 'Ok', {
        duration: 4000
      });
    });
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

  openDialog(problemId): void {
    const dialogRef = this.dialog.open(ProblemdetailsComponent, {
      width: '500px',
    });
    // console.log('problem', problemId);
    localStorage.setItem('ProblemId', problemId);
  }

  profile() {
    this.router.navigate(['/profile']);
  }

  registerProblem() {
    this.router.navigate(['/problemregister']);
  }
  
  dashboard() {
    this.router.navigate(['/userdashboard']);
  }
}
