import { Component, OnInit, ViewChild, Output, EventEmitter } from '@angular/core';
import { MatSnackBar, MatTableDataSource, MatPaginator, MatSort } from '@angular/material';

import { User, user_role } from '../shared/user';
import { AuthService } from '../services/auth.service';

import { Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'app-admin-user-actions',
  templateUrl: './admin-user-actions.component.html',
  styleUrls: ['./admin-user-actions.component.scss']
})
export class AdminUserActionsComponent implements OnInit {

  users: User[];
  user = new User();
  isEditable: boolean = false;
  editRowId: any = '';
  //isengineer = isEngineer;
  userRole = user_role;
  selectedValue: string = 'User';
  selectedValue1: string = 'User';
  errMess: string;

  // paginator input
  length: number;

  // table columns
  displayedColumns = ['username', 'firstname', 'lastname', 'userrole', 'email_id'];
  dataSource: MatTableDataSource<User>;

  constructor(
    private authService: AuthService,
    private router: Router,
    public snackBar: MatSnackBar
  ) { }

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  // @Output() userroleChange = new EventEmitter<User[]>();

  ngOnInit() {
    const data = this.selectedValue1;
    // console.log(data);
    this.authService.getSelectedUsers(data).subscribe(users => {
      this.users = users;
      this.dataSource = new MatTableDataSource(this.users);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
      
      this.length = this.users.length;
      // console.log(this.users);
    }, errmess => {
      this.errMess = errmess;
      this.snackBar.open(this.errMess, "Ok", {
        duration: 4000
      });
    });
  }

  public onSearch() {
    const data = this.selectedValue1;
    // console.log(data);
    this.authService.getSelectedUsers(data).subscribe(users => {
      this.users = users;
      this.dataSource = new MatTableDataSource(this.users);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
      this.length = this.users.length;
      // console.log(this.users);
    }, errmess => {
      this.errMess = errmess;
      this.snackBar.open(this.errMess, "Ok", {
        duration: 4000
      });
    });
    
  }

  onEdit(id: string) {
    this.isEditable = true;
    if (this.isEditable === true) {
      this.editRowId = id;
      this.user = new User();
      // console.log('editrowId', this.editRowId);
      // console.log('id', id);
    }
  }

  onSave(id: string) {
    var data1 = this.selectedValue;
    var data = this.selectedValue1;
    // console.log(data1);
    this.user.usertype = data1;
    // console.log('user', this.user);
    //For Updating User Role
    this.authService.updateUser(id, this.user).subscribe( user => {
      this.user = user;
      this.dataSource = new MatTableDataSource(this.users);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
      this.length = this.users.length;
      // console.log(user);
      this.snackBar.open(this.user.username+" is now "+this.user.usertype+"!", "Ok", {
        duration: 2000
      });
    },
    errmess => {
      this.errMess = <any>errmess;
      this.snackBar.open("Failed to edit User Role!", "Ok", {
        duration: 2000
      });
    });
    this.editRowId = '';
    this.isEditable = false;
    this.authService.getSelectedUsers(data).subscribe(users => {
      this.users = users;
      this.dataSource = new MatTableDataSource(this.users);
      this.length = this.users.length;
    });    
  }

  onCancel() {
    this.isEditable = false;
    this.editRowId = null;
  }

  onDelete(id: string) {
    this.authService.deleteUser(id).subscribe(res =>{
      // console.log('user', res);
      this.user = res;
      if(res) {
        var data = this.selectedValue;
        this.authService.getSelectedUsers(data).subscribe(users => {
          this.users = users;
          this.dataSource = new MatTableDataSource(this.users);
          this.dataSource.paginator = this.paginator;
          this.dataSource.sort = this.sort;
          this.length = this.users.length;
          this.snackBar.open("The User "+this.user.username+" is deleted successfully!", "Ok", {
            duration: 2000
          });
        },
        errmess => {
          this.errMess = <any>errmess;
          this.snackBar.open("Couldn't delete User "+this.user.username+"!", "Ok", {
            duration: 2000
          });
        });
      }
    },
    errmess => {
      this.errMess = <any>errmess;
      this.snackBar.open(this.errMess, "Ok", {
        duration: 4000
      });
    });
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

}
