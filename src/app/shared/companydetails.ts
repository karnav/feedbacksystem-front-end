export class CompanyDetails {
    company_name: string;
    company_address: string;
    company_pincode: number;
    company_city: string;
    company_state: string;
    company_country: string;
}