export class User {
    _id: string;
    firstname: string;
    lastname: string;
    //mobno: number;
    //email: string;
    username: string;
    password: string;
    usertype: string;
    // admin: boolean;
    // employee: boolean;
   // cpassword: string;
   email_id: string;
   contact_no: string;
   company_name: string;
   company_address: string;
   company_pincode: string;
   company_city: string;
   company_state: string;
   company_country: string;
};

export const user_role = [{value: 'Engineer'},{value: 'User'},{value: 'Senior Engineer'}, {value: 'Manager'}];
export const company_city = [{value: 'Mumbai'}, {value: 'Delhi'}, {value: 'Chennai'}, {value: 'Pune'}, {value: 'Vadodara'}, {value: 'Bangalore'}]
//export const isEngineer = [{value: 'true'}, {value: 'false'}];