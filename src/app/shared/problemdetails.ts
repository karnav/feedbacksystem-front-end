export class ProblemDetail {
    _id: string;
    caseId: string;
    calltype: string;
    customer: string;
    engineer: string;
    // customer_name: string;
    // customer_company_name: string;
    // contact_no: number;
    // email_id: string;
    // pincode: number;
    device_type: string;
    oem: string;
    model_no: string;
    part_no: string;
    serial_no: string;
    problem_title: string;
    problem_description: string;
    problem_status: string;
    createdAt: string;
    updatedAt: string;
    //solution: string;
} ;

export const CallType = [{value: 'Repair'}, 
    {value: 'Email'},
    {value: 'Call'},
    {value: 'Demo'},
    {value: 'Other'}
];

export const Type = [{value: 'Scanner'},{value: 'Printer'},{value: 'HHT'}];

// export const OEM = [{value:'a'},{value: 'b'},{value: 'c'}];

export const Model_no = [{value:'a'},{value: 'b'},{value: 'c'}];

export const Serial_no = [{value:'a'},{value: 'b'},{value: 'c'}];

export const ProblemStatus = [
    {value: 'Registered', viewValue: 'Registered'},
    {value: 'Waiting_for_Approval', viewValue: 'Waiting for Approval'},
    {value: 'Work_in_Progress', viewValue: 'Work in Progress'},
    {value: 'Escaleted', viewValue: 'Escaleted'},
    {value: 'Close', viewValue: 'Close'}
];

export class OEM {
    _id: string;
    name: string;
}
