import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EngineerdashboardComponent } from './engineerdashboard.component';

describe('EngineerdashboardComponent', () => {
  let component: EngineerdashboardComponent;
  let fixture: ComponentFixture<EngineerdashboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EngineerdashboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EngineerdashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
