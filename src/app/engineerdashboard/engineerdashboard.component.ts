import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { MatPaginator, MatTable, MatSnackBar, MatTableDataSource, MatSort, MatDialog, MatDialogRef } from '@angular/material';

import { ProblemService } from '../services/problem.service';

import { ProblemDetail } from '../shared/problemdetails';
import { ProblemTransaction } from '../shared/problemtransactions';
import { User } from '../shared/user';
import { Query } from '../shared/query';

@Component({
  selector: 'app-engineerdashboard',
  templateUrl: './engineerdashboard.component.html',
  styleUrls: ['./engineerdashboard.component.scss']
})
export class EngineerdashboardComponent implements OnInit {

  problems: ProblemDetail[];
  problemtransactions: ProblemTransaction[];
  users: User[];
  startDate: Date;
  endDate: Date;
  errMess: string;
  query: Query;
  Query: string;

  // paginator input
  length: number;
  
  // table columns
  displayedColumns = ['createdAt', 'problem_title', 'problem_status', 'customer_name', 'engineer', 'updatedAt'];
  dataSource: MatTableDataSource<ProblemDetail>;

  constructor(
    private router: Router,
    private problemService: ProblemService,
    private snackBar: MatSnackBar
  ) { }

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  ngOnInit() {
    // this.problemService.getProblems().subscribe(problems => {
    //   this.problems = problems;
    //   console.log('problems', problems);
    // })
    let a = 'updatedAt: \'{$gt:2017-11-07T18:30:00.000Z,$lt:2017-11-27T18:30:00:000Z}\', engineer: \'5a13da691bce1b00dc03e70e\'';
    let s = a.slice(0, 11) + a.slice(12);
    let b = s.slice(0, 70) + s.slice(71);
    // console.log('s',s);
    // console.log('a',a);
    // console.log('b',b);
  }

  profile() {
    this.router.navigate(['/profile']);
  }

  dashboard() {
    this.router.navigate(['/engineerdashboard']);
  }

  listproblem() {
    this.router.navigate(['/listproblem']);
  }

  onSubmit() {
    // var startdate =this.startDate;
    const startdate = new Date(this.startDate);
    const sdate = startdate.toISOString();
    const Id = localStorage.getItem('Id');
    const tid = Id.substr(1);
    const id = tid.slice(0, -1);
    // console.log('startdate',sdate);

    // var enddate = this.endDate;
    const enddate = new Date(this.endDate);
    const edate = enddate.toISOString();
    // console.log(startdate);
    // console.log(enddate);
    // console.log('enddate', edate);
    //  this.query.query_string= '{updatedAt:{$gte:\'' + sdate  + '\',$lte:\'' + sdate + '\'},engineer: \'' + id + '\'}';
    // start editing from here
    // var query = '?{"updatedAt":{$gte:new ISODate("'+sdate+'"),$lte:new ISODate("'+edate+'")}}';
    // this.query.query_string = this.Query;
    // console.log('query class', this.query);
    const problemstatus = '';
    this.problemService.getProblemsbyDate(sdate, edate, id, problemstatus).subscribe( problems => {
      this.problems = problems;
      this.dataSource = new MatTableDataSource(this.problems);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
      this.length = this.problems.length;
      // console.log('Problems:', problems);
    },
    errmess => {
      this.errMess = <any>errmess;
      this.snackBar.open(this.errMess, 'Ok', {
        duration: 4000
      });
    });

  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

}
