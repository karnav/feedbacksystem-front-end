import { Component, ViewChild, OnInit } from '@angular/core';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatPaginator, MatTable, MatSnackBar, MatTableDataSource, MatSort, MatDialog, MatDialogRef } from '@angular/material';

import { AuthService } from '../services/auth.service';
import { ProblemService } from '../services/problem.service';

import { Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';

import { ProblemDetail, ProblemStatus } from '../shared/problemdetails';
import { ProblemTransaction } from '../shared/problemtransactions';
import { User } from '../shared/user';
import { Mail } from '../shared/mail';
import { ProblemdetailsComponent } from '../user-dashboard/problemdetails/problemdetails.component';

@Component({
  selector: 'app-listproblem',
  templateUrl: './listproblem.component.html',
  styleUrls: ['./listproblem.component.scss']
})
export class ListproblemComponent implements OnInit {

   problems: ProblemDetail[];
   problemtransaction = new  ProblemTransaction();
   errMess: string;
   editable: boolean = false;

   editRowId: any = '';
  //  pageSize = 10;
  searchForm: FormGroup;
   problemStatus = ProblemStatus;
   selectedValue: string = 'Registered';
   selectedValue1: string = 'Registered';
  problem = new ProblemDetail();
  user: User;
  mail = new Mail();
  eng: string;
  engusername: string;
  problemCreatedDate: Date;

  // paginator input
  length: number;
  
  // table columns
  displayedColumns = ['createdAt', 'problem_title', 'problem_status', 'customer_name', 'email_id', 'problem_details'];
  dataSource: MatTableDataSource<ProblemDetail>;

  constructor(
    private fb: FormBuilder,
    private authService: AuthService,
    private router: Router,
    private problemService: ProblemService,
    public snackBar: MatSnackBar,
    public dialog: MatDialog
  ) {
    //this.createForm();
    
    
  }

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  ngOnInit() {
    // console.log('INitial', this.problem);
    this.problemService.getRegisteredProblems().subscribe(problems => {
      this.problems = problems;
      // console.log(problems);
      this.dataSource = new MatTableDataSource(this.problems);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
      this.length = problems.length;
   
    }, 
      errmess => this.errMess = <any>errmess); 
    
    // to get details of engineer
    this.eng = localStorage.getItem('Id');
    const eid = this.eng.substr(1);
    const EID = eid.slice(0, -1);
    this.authService.getUserById(EID).subscribe(user => {
      this.user = user;
      this.engusername = this.user.firstname + ' ' + this.user.lastname;
    },
    errmess => {
      this.errMess = <any>errmess;
    });
  }

  onSubmit() {
    var data = this.selectedValue;
    //  console.log(data);

    this.problemService.getSelectedProblems(data).subscribe(problems => {
      this.problems = problems;
      this.dataSource = new MatTableDataSource(this.problems);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
      this.length = problems.length;
    },
      errmess => {
        this.errMess = <any>errmess;
        this.snackBar.open(this.errMess, 'Ok', {
          duration: 4000
        });
      });
  }

  onEdit(id: string) {
    //this.editable = !this.editable ;
    this.editRowId = id;
    // console.log(this.editRowId);
  }

  onSave(id: string) {
    // console.log('Id',id);
    var data: string = this.selectedValue1;
    this.problem = new ProblemDetail;
    this.problem.problem_status = data;
    // console.log('problem',this.problem);

    //Engineer Id
    const Eid = localStorage.getItem('Id');
    // console.log('Eng Id', Eid);
    const eid = Eid.substr(1);
    const EID = eid.slice(0, -1);

    



    //For Updating problem_status
    this.problemService.updateProblem(id, this.problem, EID).subscribe(problem => {
      this.problem = problem;
      this.dataSource = new MatTableDataSource(this.problems);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
      this.length = this.problems.length;

      //customer Id 
      const Cid = this.problem.customer;
      // console.log('Customer Id', Cid);      
      // console.log(problem);
      this.problemCreatedDate = new Date(this.problem.createdAt);
      const year = this.problemCreatedDate.getFullYear();
      const month = this.problemCreatedDate.getMonth() + 1;
      const day = this.problemCreatedDate.getDate();
      const date = day + '/' + month + '/' + year;

      // geting user for sending mail
      this.authService.getUserById(Cid).subscribe(user => {
        this.user = user;
        // console.log(this.user);
        if(user) {
          this.mail.subject = 'Problem status updated';
          this.mail.message = 'Hello ' + this.user.username + ', your problem having title \'' + this.problem.problem_title + '\' which was registered on \'' + date + '\' has been taken by our support engineer. Our support engineer named \'' + this.engusername + '\' has set status of your problem to \'' + this.problem.problem_status + '\'!';
          //For sending mail
          this.problemService.sendMail(this.user, this.mail).subscribe(data => {
            // console.log("Mail", data);
            this.snackBar.open("Mail sent successfullly!", "Ok", {
              duration: 3000
            });
          },
          error => {
            // console.log(error.status, error.message);
            this.snackBar.open("Failed to send mail problem", "Ok", {
              duration: 3000
            });
          });
        }
      },
      errmess => {
        this.errMess = <any>errmess;
        this.snackBar.open(this.errMess, 'Ok', {
          duration: 4000
        });
      });

      id = '';
      this.editRowId = '';
      // console.log('id', id);
      this.problemService.getSelectedProblems(this.selectedValue).subscribe(problems => {
        this.problems = problems;
        this.dataSource = new MatTableDataSource(this.problems);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
        this.length = problems.length;

      },
      errmess => {
        this.errMess = <any>errmess;
        this.snackBar.open(this.errMess, 'Ok', {
          duration: 4000
        });
      });
      this.snackBar.open("Problem edited successfully!", "Ok", {
        duration: 2000
      });

      //for updating to problemtransaction
      this.problemService.postProblemtransaction(this.problem, EID, Cid).subscribe(problemtransaction => {
        this.problemtransaction = problemtransaction;
        // console.log('problemtransaction:', this.problemtransaction);
      });

      // var id: string = problem.customer;
      // var tempId = id.substr(0);
      // console.log(tempId);

      // this.authService.getUserById(tempId).subscribe(user => {
      //   this.user = user;
      //   console.log(this.user);
      //   if(user) {
      //     this.problemService.sendMail(this.user).subscribe(data => {
      //       console.log("Mail", data);
      //       this.snackBar.open("Successfully sent mail to"+this.user.email_id+"!", "Ok", {
      //         duration: 3000
      //       });
      //     },
      //     error => {
      //       console.log(error.status, error.message);
      //       this.snackBar.open("Couldn't send mail to"+this.user.email_id+"!", "Ok", {
      //         duration: 3000
      //       });
      //     });
      //   }
      // },
      // errmess => {
      //   this.errMess = <any>errmess;
      // });
    },
    
    errmess => {
      this.errMess = <any>errmess
      this.snackBar.open("Something went wrong!", "Ok", {
        duration: 2000
      });
    });
    
  }

  onCancel() {
    
    this.editRowId = null;
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

  openDialog(problemId): void {
    const dialogRef = this.dialog.open(ProblemdetailsComponent, {
      width: '500px',
    });
    // console.log('problem', problemId);
    localStorage.setItem('ProblemId', problemId);
  }

  // profile() {
  //   this.router.navigate(['/profile']);
  // }
  
  dashboard() {
    this.router.navigate(['/engineerdashboard']);
  }
  
  listproblem() {
    this.router.navigate(['/listproblem']);
  } 

}



