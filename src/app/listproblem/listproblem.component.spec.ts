import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListproblemComponent } from './listproblem.component';

describe('ListproblemComponent', () => {
  let component: ListproblemComponent;
  let fixture: ComponentFixture<ListproblemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListproblemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListproblemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
