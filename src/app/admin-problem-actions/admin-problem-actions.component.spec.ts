import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminProblemActionsComponent } from './admin-problem-actions.component';

describe('AdminProblemActionsComponent', () => {
  let component: AdminProblemActionsComponent;
  let fixture: ComponentFixture<AdminProblemActionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminProblemActionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminProblemActionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
