import { Component, OnInit, ViewChild } from '@angular/core';
import { MatSnackBar, MatTableDataSource, MatPaginator, MatSort } from '@angular/material';

import { ProblemDetail, ProblemStatus } from '../shared/problemdetails';
import { ProblemService } from '../services/problem.service';

import { Router } from '@angular/router';
// import { DataSource } from '@angular/cdk/collections';
import { Observable } from 'rxjs/Observable';
// import { ViewChild } from '@angular/core/src/metadata/di';

@Component({
  selector: 'app-admin-problem-actions',
  templateUrl: './admin-problem-actions.component.html',
  styleUrls: ['./admin-problem-actions.component.scss']
})
export class AdminProblemActionsComponent implements OnInit {

  problemStatus = ProblemStatus;
  selectedValue: string = 'Waiting_for_Approval';
  problems: ProblemDetail[];
  errMess: string;
  // paginator input
  length: number;
 
 


  // table columns
  displayedColumns = ['createdAt', 'problem_title','problem_status', 'firstname', 'email_id', 'customer.company_name', 'actions'];
  dataSource: MatTableDataSource<ProblemDetail>;




  constructor(
    private router: Router,
    private problemService: ProblemService,
    public snackBar: MatSnackBar,
    // private TableDataSource: MatTableDataSource
  ) { }

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  ngOnInit() {
    const data = this.selectedValue;
    // console.log(data);

    this.problemService.getSelectedProblems(data).subscribe(problems => {
        this.problems = problems;
        // console.log('problems', this.problems);
        this.length = this.problems.length;
        this.dataSource = new MatTableDataSource(problems);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
     },
     errmess => {
       this.errMess = <any>errmess;
       this.snackBar.open("Error in fetching "+data+"!", "Ok", {
         duration: 2000
       });
     });
  }

  onSubmit() {
    var data = this.selectedValue;
    //  console.log(data);

    this.problemService.getSelectedProblems(data).subscribe(problems => {
        this.problems = problems;
        this.length = this.problems.length;
        this.dataSource = new MatTableDataSource(problems);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
        
      },
      errmess => {
        this.errMess = <any>errmess;
        this.snackBar.open("Error in fetching "+data+"!", "Ok", {
          duration: 2000
        });
      });
      
  }

  

  onDelete(id: string) {
    // console.log('pid', id);
    this.problemService.deleteProblem(id).subscribe(res =>{
      if(res) {
        var data = this.selectedValue;
        this.problemService.getSelectedProblems(data).subscribe(problems => {
          this.problems = problems;
          this.length = problems.length;
          this.dataSource = new MatTableDataSource(problems);
          this.dataSource.paginator = this.paginator;
          this.dataSource.sort = this.sort;
          this.snackBar.open("Problem Deleted successfully!", "Ok", {
            duration: 2000
          });
        },
          errmess => {
            this.errMess = <any>errmess;
            this.snackBar.open("Error in deleting Problem!", "ok", {
              duration: 2000
            });
          });
      }
    },
    errmess => {
      this.errMess = <any>errmess;
      this.snackBar.open(this.errMess, "Ok", {
        duration: 4000
      });
    });

  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

}

