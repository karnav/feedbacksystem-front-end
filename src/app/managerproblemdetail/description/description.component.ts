import { Component, OnInit } from '@angular/core';
import {  MatDialog, MatDialogRef } from '@angular/material';
import { ProblemDetail, ProblemStatus } from '../../shared/problemdetails';
import { ProblemService } from '../../services/problem.service';


@Component({
  selector: 'app-description',
  templateUrl: './description.component.html',
  styleUrls: ['./description.component.scss']
})
export class DescriptionComponent implements OnInit {

  problem: ProblemDetail;

  constructor(
    private problemService: ProblemService
  ) { }

  ngOnInit() {
    const pid = localStorage.getItem('ProblemId');
    // console.log(pid);
    this.problemService.getProblem(pid).subscribe(problem => {
      this.problem = problem;
      // console.log('yu', this.problem);
    });
  }

}
