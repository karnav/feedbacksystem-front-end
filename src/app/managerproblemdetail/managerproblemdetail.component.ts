import {Component, ViewChild, AfterViewInit, Inject} from '@angular/core';
import {MatPaginator, MatTableDataSource, MatSort, MatDialog, MatSnackBar, MatDialogRef, MAT_DIALOG_DATA, MatDialogModule} from '@angular/material';

import { ProblemDetail, ProblemStatus } from '../shared/problemdetails';
import { DescriptionComponent } from './description/description.component';
import { ProblemService } from '../services/problem.service';

import { Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'app-managerproblemdetail',
  templateUrl: './managerproblemdetail.component.html',
  styleUrls: ['./managerproblemdetail.component.scss']
})
export class ManagerproblemdetailComponent implements AfterViewInit {

  problemStatus = ProblemStatus;
  selectedValue = 'Waiting_for_Approval';
  problems: ProblemDetail[];
  errMess: string;

  displayedColumns = ['problem_title', 'problem_status'];
  // dataSource = new MatTableDataSource(ELEMENT_DATA);
  dataSource: MatTableDataSource<ProblemDetail>;

  

  constructor(
    private router: Router,
    private problemService: ProblemService,
    public dialog: MatDialog,
    private snackBar: MatSnackBar,
    // private TableDataSource: MatTableDataSource
  ) { }

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  
    /**
     * Set the sort after the view init since this component will
     * be able to query its view for the initialized sort.
     */
    // tslint:disable-next-line:use-life-cycle-interface
    ngAfterViewInit() {
      const data = this.selectedValue;
      this.problemService.getSelectedProblems(data).subscribe(problems => {
        this.problems = problems;
        // this.length = this.problems.length;
        this.dataSource = new MatTableDataSource(this.problems);
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
        // console.log('datasource', this.dataSource);
      },
      errmess => {
        this.errMess = <any>errmess;
        this.snackBar.open(this.errMess, 'Ok', {
          duration: 4000
        });
        // this.snackBar.open("Error in fetching "+data+"!", "Ok", {
        //   duration: 2000
        // });
      });
      
    }
 
  openDialog(problem): void {
    const dialogRef = this.dialog.open(DescriptionComponent, {
      width: '500px',
    });
    // console.log('problem', problem);
    localStorage.setItem('ProblemId', problem);
  }

  onSubmit() {
    const data = this.selectedValue;
    //  console.log(data);

    this.problemService.getSelectedProblems(data).subscribe(problems => {
        this.problems = problems;
        // this.length = this.problems.length;
        this.dataSource = new MatTableDataSource(this.problems);
        // console.log('datasource', this.dataSource);
      },
      errmess => {
        this.errMess = <any>errmess;
        this.snackBar.open(this.errMess, 'Ok', {
          duration: 4000
        });
        // this.snackBar.open("Error in fetching "+data+"!", "Ok", {
        //   duration: 2000
        // });
      });

  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

  onDelete(id: string) {
    this.problemService.deleteProblem(id).subscribe(res =>{
      if(res) {
        var data = this.selectedValue;
        this.problemService.getSelectedProblems(data).subscribe(problems => {
          this.problems = problems;
          this.snackBar.open("Problem Deleted successfully!", "Ok", {
            duration: 2000
          });
        },
          errmess => {
            this.errMess = <any>errmess;
            this.snackBar.open(this.errMess, "ok", {
              duration: 4000
            });
          });
      }
    },
    errmess => {
      this.errMess = <any>errmess;
      this.snackBar.open(this.errMess, "Ok", {
        duration: 2000
      });
    });

  }

  // pag1 = {
  // pageSize: 10,
  // pageSizeOptions: [5, 10, 20],
  // length: [100],
  // };

  // // constructor() { }

  



  // @ViewChild('paginator') paginator: MatPaginator;

  // /**
  //  * Set the paginator after the view init since this component will
  //  * be able to query its view for the initialized paginator.
  //  */
  // ManagerproblemdetailComponentngAfterViewInit() {
  //   this.dataSource.paginator = this.paginator;
  // }
}

// @Component({
//   selector: 'app-example-dialog',
//   templateUrl: 'example-dialog.html',
// })
// export class ExampleDialogComponent {
//   constructor(
//     public dialogRef: MatDialogRef<ExampleDialogComponent>,
//     @Inject(MAT_DIALOG_DATA) public data: any) {}
  
//   onNoClick(): void {
//     this.dialogRef.close();
//   }
// }

// export interface Element {
//   name: string;
//   position: number;
//   weight: number;
//   symbol: string;
// }

// const ELEMENT_DATA: Element[] = [
//   {position: 1, name: 'Hydrogen', weight: 1.0079, symbol: 'H'},
//   {position: 2, name: 'Helium', weight: 4.0026, symbol: 'He'},
//   {position: 3, name: 'Lithium', weight: 6.941, symbol: 'Li'},
//   {position: 4, name: 'Beryllium', weight: 9.0122, symbol: 'Be'},
//   {position: 5, name: 'Boron', weight: 10.811, symbol: 'B'},
//   {position: 6, name: 'Carbon', weight: 12.0107, symbol: 'C'},
//   {position: 7, name: 'Nitrogen', weight: 14.0067, symbol: 'N'},
//   {position: 8, name: 'Oxygen', weight: 15.9994, symbol: 'O'},
//   {position: 9, name: 'Fluorine', weight: 18.9984, symbol: 'F'},
//   {position: 10, name: 'Neon', weight: 20.1797, symbol: 'Ne'},
//   {position: 11, name: 'Sodium', weight: 22.9897, symbol: 'Na'},
//   {position: 12, name: 'Magnesium', weight: 24.305, symbol: 'Mg'},
//   {position: 13, name: 'Aluminum', weight: 26.9815, symbol: 'Al'},
//   {position: 14, name: 'Silicon', weight: 28.0855, symbol: 'Si'},
//   {position: 15, name: 'Phosphorus', weight: 30.9738, symbol: 'P'},
//   {position: 16, name: 'Sulfur', weight: 32.065, symbol: 'S'},
//   {position: 17, name: 'Chlorine', weight: 35.453, symbol: 'Cl'},
//   {position: 18, name: 'Argon', weight: 39.948, symbol: 'Ar'},
//   {position: 19, name: 'Potassium', weight: 39.0983, symbol: 'K'},
//   {position: 20, name: 'Calcium', weight: 40.078, symbol: 'Ca'},
// ];

