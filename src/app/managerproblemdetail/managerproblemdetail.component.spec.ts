import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManagerproblemdetailComponent } from './managerproblemdetail.component';

describe('ManagerproblemdetailComponent', () => {
  let component: ManagerproblemdetailComponent;
  let fixture: ComponentFixture<ManagerproblemdetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManagerproblemdetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManagerproblemdetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
