import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Params, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { User, user_role } from '../shared/user';

import { AuthService } from '../services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss']
})
export class AdminComponent implements OnInit {

  userRoles = user_role;
  selectedValue = 'Employee';
  //username: string;
  enteredUsername: string;
  user: User;
  userForm: FormGroup;

  constructor(private authService: AuthService,
    private fb: FormBuilder,
    private router: Router,
    http: HttpClient) {
      //this.createForm();
     }

  ngOnInit() {
    // var Username = this.username;
    // console.log(Username);
  }

  // createForm() {
  //   this.userForm = this.fb.group({
  //     'username': [null, Validators.required]
  //   });
  // }



  // onSearch(username: string) {
  //   var Username = username;
  //   console.log(Username);
  //   this.authService.getUser(Username).subscribe(user => { this.user = user; });
  // }

  // onEdit(){
  //   var u=this.onSearch(this.user.username);

  // }

  // OpenUserPage() {
  //   this.router.navigate(['/adminuser']);
  // }

}
