import { Component, OnInit, ViewChild} from '@angular/core';

import { AuthService } from '../services/auth.service';
import { Router } from '@angular/router';

import { MatSidenav } from '@angular/material';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  

  constructor(
    private authService: AuthService,
    private router: Router
  ) { }

  ngOnInit() {
    
  }

  // open() {
  //   this.myNav.open();
  // }

  // profile() {
  //   this.router.navigate(['/profile']);
  // }

  // dashboard() {
  //   this.router.navigate(['/engineerdashboard']);
  // }

  // listproblem() {
  //   this.router.navigate(['/listproblem']);
  // }

  // LogOut() {
  //   this.authService.logOut();
  //   this.router.navigate(['/login']);
  // }

}
