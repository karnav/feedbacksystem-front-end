import { Component, OnInit } from '@angular/core';

import { OEM } from '../shared/problemdetails';
import { OemService } from '../services/oem.service';

@Component({
  selector: 'app-admin-oem-panel',
  templateUrl: './admin-oem-panel.component.html',
  styleUrls: ['./admin-oem-panel.component.scss']
})
export class AdminOemPanelComponent implements OnInit {

  oems: OEM[];
  oeminput = '';
  oemError = false;
  errMess: string;
  public Oem = [];
  selectedOem: string;

  constructor(
    private oemService: OemService
  ) { }

  ngOnInit() {
    this.oemService.getOems().subscribe(oems => {
      this.oems = oems;
      console.log('oems', this.oems);
      for (let i = 0; i < oems.length; i++ ) {
        this.Oem[i] = {value: this.oems[i]._id, viewName: this.oems[i].name};
      }
    },
    errmess => {
      this.errMess = <any>errmess;
    });
  }

  AddOem() {
    // const input = this.oeminput;
    if ( this.oeminput.length > 0 ) {
      this.oemError = false;
    } else {
      this.oemError = true;
    }
    console.log(this.oeminput.trim());
    console.log(this.oemError);
    const oem = new OEM();
    oem.name = this.oeminput.trim();
    if ( this.oemError === false ) {
      this.oemService.postOem(oem).subscribe(oem => {
        this.oems = oem;
        console.log('oems', this.oems);
        this.oemService.getOems().subscribe(oems => {
          this.oems = oems;
          console.log('oems', this.oems);
          for (let i = 0; i < oems.length; i++ ) {
            this.Oem[i] = {value: this.oems[i]._id, viewName: this.oems[i].name};
          }
        },
        errmess => {
          this.errMess = <any>errmess;
        });
      },
      errmess => {
        this.errMess = <any>errmess;
      });
    }
  }

  changed() {
    alert('CHANGED');
  }

  DeleteOem() {
    const selectedoem = this.selectedOem;
    console.log('oem', selectedoem);
    console.log(this.oems);
    const index = this.oems.findIndex(o => {
      return o._id === selectedoem;
    });
    console.log(index);
    if ( index !== -1) {
      this.oems.splice(index, 1);
    }
    console.log('after delete', this.oems);
    this.oemService.deleteOem(selectedoem).subscribe(oem => {
      // this.oemService.getOems().subscribe(oems => {
      //   this.oems = oems;
      //   console.log('oems', this.oems);
      //   for (let i = 0; i < oems.length; i++ ) {
      //     this.Oem[i] = {value: this.oems[i]._id, viewName: this.oems[i].name};
      //   }
      // },
      // errmess => {
      //   this.errMess = <any>errmess;
      // });
      this.ngOnInit();
    },
    errmess => {
      this.errMess = <any>errmess;
    });
  }

}
