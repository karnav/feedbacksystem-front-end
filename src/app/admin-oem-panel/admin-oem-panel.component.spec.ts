import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminOemPanelComponent } from './admin-oem-panel.component';

describe('AdminOemPanelComponent', () => {
  let component: AdminOemPanelComponent;
  let fixture: ComponentFixture<AdminOemPanelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminOemPanelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminOemPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
