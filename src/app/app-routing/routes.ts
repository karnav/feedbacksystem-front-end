import { Routes } from '@angular/router';

import { AppComponent } from '../app.component';
import { LoginComponent } from '../login/login.component';
import { SignupComponent } from '../signup/signup.component';
import { ProblemregisterComponent } from '../problemregister/problemregister.component';
import { ListproblemComponent } from '../listproblem/listproblem.component';
import { ListdetailproblemComponent } from '../listdetailproblem/listdetailproblem.component';
import { AdminComponent } from '../admin/admin.component';
import { AdminUserActionsComponent } from '../admin-user-actions/admin-user-actions.component';
import { AdminProblemActionsComponent } from '../admin-problem-actions/admin-problem-actions.component';
import { UserDashboardComponent } from '../user-dashboard/user-dashboard.component';
import { UserProfileComponent } from '../user-profile/user-profile.component';
import { EngineerdashboardComponent } from '../engineerdashboard/engineerdashboard.component';
import { SEngDashboardComponent } from '../s-eng-dashboard/s-eng-dashboard.component';
import { WorklogComponent } from '../worklog/worklog.component';
import { ManagerdashboardComponent } from '../managerdashboard/managerdashboard.component';
import { ManagerlistproblemComponent } from '../managerlistproblem/managerlistproblem.component';
import { ManagerproblemdetailComponent } from '../managerproblemdetail/managerproblemdetail.component';
import { PasswordResetComponent } from '../password-reset/password-reset.component';
import { AdminOemPanelComponent } from '../admin-oem-panel/admin-oem-panel.component';
import { AuthGuard } from '../guards/auth.guard';

export const routes: Routes = [
    {path: 'login', component: LoginComponent},
    {path: 'signup', component: SignupComponent},
    {path: 'problemregister', component: ProblemregisterComponent, canActivate: [AuthGuard]},
    {path: 'listproblem', component: ListproblemComponent, canActivate: [AuthGuard]},
    {path: 'listdetailproblem/:id', component: ListdetailproblemComponent, canActivate: [AuthGuard]},
    {path: 'admin', component: AdminComponent, canActivate: [AuthGuard]},
    {path: 'adminuser', component: AdminUserActionsComponent, canActivate: [AuthGuard]},
    {path: 'adminproblem', component: AdminProblemActionsComponent, canActivate: [AuthGuard]},
    {path: 'adminoem', component: AdminOemPanelComponent},
    {path: 'userdashboard', component: UserDashboardComponent, canActivate: [AuthGuard]},
    {path: 'profile', component: UserProfileComponent, canActivate: [AuthGuard]},
    {path: 'engineerdashboard', component: EngineerdashboardComponent, canActivate: [AuthGuard]},
    {path: 'sengdashboard', component: SEngDashboardComponent, canActivate: [AuthGuard]},
    {path: 'worklog', component: WorklogComponent, canActivate: [AuthGuard]},
    {path: 'managerdashboard', component: ManagerdashboardComponent, canActivate: [AuthGuard]},
    {path: 'managerlistproblem', component: ManagerlistproblemComponent, canActivate: [AuthGuard]},
    {path: 'managerproblemdetail', component: ManagerproblemdetailComponent, canActivate: [AuthGuard]},
    {path: 'reset-password', component: PasswordResetComponent},
    {path: '', redirectTo: '/login', pathMatch: 'full'}
];
