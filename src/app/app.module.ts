import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { MatButtonModule, MatCheckboxModule, MatDatepickerModule, MatNativeDateModule, MatFormFieldModule, MatSidenavModule,
  MatInputModule, MatRadioModule, MatSelectModule, MatSliderModule, MatTooltipModule, MatSortModule, MatExpansionModule,
  MatSlideToggleModule, MatToolbarModule, MatListModule, MatGridListModule, MatSnackBarModule, MatPaginatorModule,
  MatCardModule, MatIconModule, MatProgressSpinnerModule, MatDialogModule, MatTableModule } from '@angular/material';
import { FlexLayoutModule } from '@angular/flex-layout';
import { ReactiveFormsModule } from '@angular/forms';


import 'hammerjs';

import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { SignupComponent } from './signup/signup.component';
import { ProblemregisterComponent } from './problemregister/problemregister.component';
import { ListproblemComponent } from './listproblem/listproblem.component';
import { ListdetailproblemComponent } from './listdetailproblem/listdetailproblem.component';
import { AdminComponent } from './admin/admin.component';
import { HeaderComponent } from './header/header.component';
import { AdminUserActionsComponent } from './admin-user-actions/admin-user-actions.component';
import { AdminProblemActionsComponent } from './admin-problem-actions/admin-problem-actions.component';
import { UserProfileComponent } from './user-profile/user-profile.component';
import { UserDashboardComponent } from './user-dashboard/user-dashboard.component';
import { EngineerdashboardComponent } from './engineerdashboard/engineerdashboard.component';
import { SEngDashboardComponent } from './s-eng-dashboard/s-eng-dashboard.component';
import { WorklogComponent } from './worklog/worklog.component';
import { ManagerdashboardComponent } from './managerdashboard/managerdashboard.component';
import { ManagerlistproblemComponent } from './managerlistproblem/managerlistproblem.component';
import { ManagerproblemdetailComponent } from './managerproblemdetail/managerproblemdetail.component';
import { DescriptionComponent } from './managerproblemdetail/description/description.component';
import { ProblemdetailsComponent } from './user-dashboard/problemdetails/problemdetails.component';

import { AppRoutingModule } from './app-routing/app-routing.module';

import { baseURL } from './shared/baseurl';
import { AuthService } from './services/auth.service';
import { AuthGuard } from './guards/auth.guard';
// import { AuthInterceptor, UnauthorizedInterceptor } from './services/auth.interceptor';
import { ProcessHttpmsgService } from './services/process-httpmsg.service';
import { ProblemService } from './services/problem.service';
import { OemService } from './services/oem.service';
import { SerialNoService } from './services/serial-no.service';
import { RestangularModule, Restangular } from 'ngx-restangular';
import { RestangularConfigFactory } from './shared/restConfig';
import {HTTP_INTERCEPTORS} from '@angular/common/http';
import { PasswordResetComponent } from './password-reset/password-reset.component';
import { AdminOemPanelComponent } from './admin-oem-panel/admin-oem-panel.component';



@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    SignupComponent,
    ProblemregisterComponent,
    ListproblemComponent,
    AdminComponent,
    ListdetailproblemComponent,
    HeaderComponent,
    AdminUserActionsComponent,
    AdminProblemActionsComponent,
    UserProfileComponent,
    UserDashboardComponent,
    EngineerdashboardComponent,
    SEngDashboardComponent,
    WorklogComponent,
    ManagerdashboardComponent,
    ManagerlistproblemComponent,
    ManagerproblemdetailComponent,
    DescriptionComponent,
    ProblemdetailsComponent,
    PasswordResetComponent,
    AdminOemPanelComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpClientModule,
    MatButtonModule, MatCheckboxModule, MatDatepickerModule, MatNativeDateModule, MatFormFieldModule, MatSidenavModule,
    MatInputModule, MatRadioModule, MatSelectModule, MatSliderModule, MatTooltipModule, MatSortModule,
    MatSlideToggleModule, MatToolbarModule, MatListModule, MatGridListModule, MatSnackBarModule, MatPaginatorModule,
    MatCardModule, MatIconModule, MatProgressSpinnerModule, MatDialogModule, MatTableModule, MatExpansionModule,
    FlexLayoutModule,
    AppRoutingModule,
    ReactiveFormsModule,
    RestangularModule.forRoot(RestangularConfigFactory)
  ],
  entryComponents: [
    DescriptionComponent,
    ProblemdetailsComponent
  ],
  providers: [
    AuthService,
    AuthGuard,
    ProblemService,
    ProcessHttpmsgService,
    OemService,
    SerialNoService,
    { provide: 'BaseURL', useValue: baseURL },
    /*{
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: UnauthorizedInterceptor,
      multi: true
    }*/
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
