import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListdetailproblemComponent } from './listdetailproblem.component';

describe('ListdetailproblemComponent', () => {
  let component: ListdetailproblemComponent;
  let fixture: ComponentFixture<ListdetailproblemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListdetailproblemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListdetailproblemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
