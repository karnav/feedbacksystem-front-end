import { Component, OnInit } from '@angular/core';

import {ProblemDetail} from '../shared/problemdetails';
import { ProblemService } from '../services/problem.service';

import { Params, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

import 'rxjs/add/operator/switchMap';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-listdetailproblem',
  templateUrl: './listdetailproblem.component.html',
  styleUrls: ['./listdetailproblem.component.scss']
})
export class ListdetailproblemComponent implements OnInit {

  problem: ProblemDetail;
  problemIds: String[];
  prev: String;
  next: String;
  errMess: string;

  constructor(
    private route: ActivatedRoute,
    private location: Location,
    private problemService: ProblemService,
    private snackBar: MatSnackBar
  ) { }

  ngOnInit() {
    //let id = +this.route.snapshot.params['id'];
    //this.problemService.getProblem(id).subscribe(problem => this.problem =problem);
    //this.problemService.getProblemIds().subscribe(problemIds => this.problemIds = problemIds);
    //var id = this.route.snapshot.params['id'];
    //console.log(id);
    //this.problemService.getProblem(id).subscribe(problem => this.problem = problem);
    this.route.params
      .switchMap((params: Params) => {return this.problemService.getProblem(params['id']); })
      .subscribe(problem => { 
        this.problem = problem;
        // console.log(this.problem);
      }, 
      errmess => {
        this.errMess = <any>errmess;
        this.snackBar.open(this.errMess, 'Ok', {
          duration: 4000
        });
      });
    
  }

  // setPrevNext(problemId: String) {
  //   if(this.problemIds) {
  //     let index = this.problemIds.indexOf(problemId);
  //     this.prev = this.problemIds[(this.problemIds.length + index - 1)%this.problemIds.length];
  //     this.next = this.problemIds[(this.problemIds.length + index + 1)%this.problemIds.length];
  //   }
  // }

  goBack(): void {
    this.location.back();
  }
}
