import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material';
import { User } from '../shared/user';
import { Router } from '@angular/router';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {

  signupForm: FormGroup;
  user: User;
  firstname: string;

  formErrors = {
    'firstname': '',
    'lastname': '',
    /*'mobno': '',
    'email': '',*/
    'username': '',
    'password': ''
  };
  
  validationMessages = {
    'firstname': {
      'required': 'First name is required.'
    },
    'lastname': {
      'required': 'Last name is required.'
    },
    /*'mobno': {
      'pattern': 'Mobile number must contain only numbers.'
    },
    'email': {
      'required': 'Email is required.',
      'email': 'Email is not in valid format.'
    },*/
    'username': {
      'required': 'Please enter Username.',
      'minlength': 'Username should be atleast 2 characters',
      'maxlength': 'Username should not exceed more than 20 characters'
    },
    'password': {
      'required': 'Password is required.',
      'minlength': 'Password must be of atleast 8 characters',
      'maxlength': 'Password should not exceed more than 25 characters'
    },
  };



  constructor(private fb: FormBuilder, 
    private router: Router, 
    private authService: AuthService,
    public snackBar: MatSnackBar) { 
    this.createForm();
  }

  ngOnInit() {
  }

  createForm() {
    this.signupForm = this.fb.group({
      firstname: ['', Validators.required],
      lastname: ['', Validators.required],
      /*mobno: ['', Validators.pattern],
      email: ['', Validators.email],*/
      username: ''/* Validators.required, Validators.minLength(2), Validators.maxLength(20)]*/,
      password: ''/* Validators.required, Validators.minLength(8), Validators.maxLength(25)]*/
    });

    this.signupForm.valueChanges.subscribe(data => this.onValueChanged(data));
    this.onValueChanged();
  }

  onValueChanged(data?: any) {
    if (!this.signupForm) { return; }
    const form = this.signupForm;
    for (const field in this.formErrors) {
      // clear previous error message (if any)
      this.formErrors[field] = '';
      const control = form.get(field);
      if (control && control.dirty && !control.valid) {
        const messages = this.validationMessages[field];
        for (const key in control.errors) {
          this.formErrors[field] += messages[key] + ' ';
        }
      }
    }
  }

  onSubmit() {
    this.user = this.signupForm.value;
    // console.log(this.user);
    this.authService.signUp(this.user).subscribe(res => {
      if (res.success) {
        this.router.navigate(['/login']).then(() => {
          this.snackBar.open("Successfully signed up!", "Ok", {
            duration: 2000
          });
        });
      } else {
        this.router.navigate(['/signup']).then(() => {
          this.snackBar.open("Sign up failed, Kindly sign up again!", "Ok", {
            duration: 3000
          });
        });
      }
    });
  }

}
